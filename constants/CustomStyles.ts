import { StyleSheet, Platform, StatusBar } from 'react-native';

const primaryColor: String = '#81E8B2';

const LoginStyles = StyleSheet.create({
    loginContainer: {
      flex: 1,
      backgroundColor: '#81E8B2',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
    loginContent: {
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      borderBottomLeftRadius: 30,
      borderBottomRightRadius: 30,
      paddingVertical: 50,
      paddingHorizontal: 30,
      margin: 20
    },
    loginTitle: {
      fontSize: 30,
      fontWeight: 'bold',
      paddingBottom: 20
    },
    
});

const UtilsStyles = StyleSheet.create({
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionCustom: {
        marginTop: 10,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    placeholderCustom: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#666666',
      },
    actionPicker: {
        marginTop: 10,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
});

const ContentStyles = StyleSheet.create({
    topSafeAreaView: {
        flex: 0, 
        backgroundColor: '#81E8B2'
    },
    maincontainer: {
        flex: 1, 
        marginTop: StatusBar.currentHeight, 
        backgroundColor: '#FFFFFF'
    },
    whiteBackgroundColor: {
        backgroundColor: '#FFFFFF',
        padding: 20,
        flex: 1
    },
    customNavBar: {
        flexDirection: 'row', 
        backgroundColor: '#81E8B2'
    },
    menuImg: {
        backgroundColor: '#81E8B2', 
        borderBottomLeftRadius: 150, 
        borderBottomRightRadius: 150, 
        justifyContent: 'flex-start', 
        alignItems: 'center', 
        paddingBottom: 30, 
        paddingTop: 20
    },
    menuImgLeftSide: {
        backgroundColor: '#81E8B2', 
        borderBottomLeftRadius: 300, 
        borderBottomRightRadius: 0,
        paddingBottom: 50, 
        paddingTop: 20
    },
    menuImgLeftSide1: {
        backgroundColor: '#81E8B2', 
        borderBottomLeftRadius: 300, 
        borderBottomRightRadius: 0,
        paddingBottom: 20, 
        paddingTop: 20
    },
    titleText: {
        fontSize: 25, 
        fontWeight: 'bold', 
        color: '#FFF'
    },
    contentAbsolute: {
        position: 'absolute', 
        top: '30%', 
        left: 0, 
        right: 0, 
        bottom: 0, 
        backgroundColor: 'transparent'
    },
    appButton: {
        elevation: 8,
        backgroundColor: "#ffbd59",
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 10 ,
        marginBottom:10       
      }
});

export { 
    LoginStyles, 
    UtilsStyles,
    ContentStyles
};