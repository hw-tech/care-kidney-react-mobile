import * as React from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList, TouchableOpacity } from 'react-native';

import { Text, View } from '../components/Themed';
import { Ionicons } from '@expo/vector-icons';

export default function IngredientInfoScreen({ navigation } : { navigation: any }) {
  const numColumns = 1;
  const itemList = [  
    {key: '1', name: 'เกลือ', sodium: 1500},
    {key: '2', name: 'น้ำปลา', sodium: 1200},
    {key: '3', name: 'ผงชูรส', sodium: 2000},
    {key: '4', name: 'ซีอิ้วขาว', sodium: 750},
  ];

  function renderItem({ item } : { item: any }) {
    return (
      <TouchableOpacity onPress={() => {navigation.push('IngredientInfo.Detail')}} >
        <View style={{height: 50, flexDirection: 'row', marginBottom: 10, borderBottomWidth: 0.5}}>
            <Image
              style={{height: 50, width: 50}}
              source={require('./../assets/images/ingredient.png')}
            />
          <Text style={{paddingHorizontal: 20, alignSelf: 'center', fontSize: 18}}>{`${item.name} (ปริมาณโซเดียม ${item.sodium})`}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  return (
    <React.Fragment>
      <SafeAreaView style={{ flex: 0, backgroundColor: '#CE9C6F' }} />
      <View style={{ backgroundColor: '#CE9C6F' }}>
        <Ionicons name="ios-arrow-back" size={40} style={{ paddingLeft: 20 }} onPress={() => navigation.goBack()}/>
      </View>
      <SafeAreaView style={styles.container}>
        <View style={styles.space}>
          <Image
            style={styles.logo}
            source={require('./../assets/images/ingredient.png')}
          />
          <Text style={styles.title_header}>ข้อมูลเครื่องปรุง</Text>
        </View>
        <View style={styles.content}>
          <FlatList data={itemList} renderItem={renderItem} numColumns={numColumns} />  
        </View>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CE9C6F'
  },
  space: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CE9C6F',
    margin: 10,
  },
  content: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingHorizontal: 30
  },
  title_header: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#FFF',
  },
  logo: {
    width: 100,
    height: 100,
  },
});
