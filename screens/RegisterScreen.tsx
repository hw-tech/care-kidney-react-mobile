import React, { useState } from 'react';
import { StyleSheet, Button, TextInput, SafeAreaView, ScrollView, Picker, StatusBar, Image } from 'react-native';

import { Text, View } from '../components/Themed';
import { ContentStyles, UtilsStyles } from '../constants/CustomStyles';
import { Ionicons } from '@expo/vector-icons';

import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { ROOT_URL } from '../constants/Variable';

export default function RegisterScreen({ navigation } : { navigation: any }) {

  const [userObj, setUserObj] = useState({
    username: "",
    password: "",
    rePassword: "",
    titleName: "",
    firstname: "",
    lastname: "",
    identificationId: "",
    process: "",
    createUser: "",
    updateUser: ""
  });
  const test = async () => {
    console.log("test");
    
  };

  const onRegister = async () => {
    userObj.process = "RegisterScreen";
    userObj.createUser = userObj.username;

    if(userObj.password !== userObj.rePassword){
      Toast.show('รหัสผ่านกับยืนยันรหัสผ่านไม่เหมือนกัน', Toast.SHORT);
      return;
    }

    if(!userObj.username || !userObj.password || !userObj.rePassword || !userObj.firstname || !userObj.lastname || !userObj.identificationId){
      Toast.show('กรุณากรอกข้อมูลให้ครบถ้วน', Toast.SHORT);
      return;
    }
    // console.log(userObj.username);

    // if(userObj.identificationId.length != 13){
    //   Toast.show('เลขบัตรประชาชนไม่ครบหรือเกิน 13 หลัก', Toast.SHORT);
    //   return;
    // }

    let res = await axios.get(`${ROOT_URL}/users/${userObj.username}/${userObj.identificationId}`);

    //  console.log(res.data);

    if(res.data.length == 0){
      axios.post(`${ROOT_URL}/users/`, userObj)
      .then(response => {
        if(response.status == 200){
          navigation.replace('Login');
        }
      }, (error) => {
        console.log(error);
      });
    } else {
      Toast.show('ชื่อผู้ใช้งานหรือเลขบัตรประชาชนซ้ำ', Toast.SHORT);
    }
  };

  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()}/>
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 40, height: 40, alignSelf: 'center' }}
              source={require('./../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75 }}
            source={require('./../assets/images/account.png')}
          />
          <Text style={ContentStyles.titleText}>สมัครสมาชิก</Text>
        </View>
        <View style={ContentStyles.whiteBackgroundColor}>
          <ScrollView>
            <Text>ชื่อผู้ใช้งาน</Text>
            <View style={UtilsStyles.action}>
              <TextInput 
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                onChangeText={(text)=>{
                  setUserObj({...userObj, username: text})
                }}
              />
            </View>
            <Text>รหัสผ่าน</Text>
            <View style={UtilsStyles.action}>
              <TextInput 
                secureTextEntry={true}
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                onChangeText={(text)=>{
                  setUserObj({...userObj, password: text})
                }}
              />
            </View>
            <Text>ยืนยันรหัสผ่าน</Text>
            <View style={UtilsStyles.action}>
              <TextInput 
                secureTextEntry={true}
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                onChangeText={(text)=>{
                  setUserObj({...userObj, rePassword: text})
                }}
              />
            </View>
            <Text>คำนำหน้า</Text>
            <View style={UtilsStyles.actionPicker}>
              <Picker
                selectedValue={userObj.titleName}
                style={{}}
                onValueChange={(itemValue, itemIndex) => {
                  setUserObj({...userObj, titleName: itemValue});
                }} >
                <Picker.Item label="กรุณาเลือก..." value="" />
                <Picker.Item label="นาย" value="นาย" />
                <Picker.Item label="นาง" value="นาง" />
                <Picker.Item label="นางสาว" value="นางสาว" />
              </Picker>
            </View>
            <Text>ชื่อ</Text>
            <View style={UtilsStyles.action}>
              <TextInput 
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                onChangeText={(text)=>{
                  setUserObj({...userObj, firstname: text})
                }}
              />
            </View>
            <Text>นามสกุล</Text>
            <View style={UtilsStyles.action}>
              <TextInput 
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                onChangeText={(text)=>{
                  setUserObj({...userObj, lastname: text})
                }}
              />
            </View>
            {/* <Text>เลขบัตรประชาชน</Text>
            <View style={UtilsStyles.action}>
              <TextInput 
                maxLength={13}
                keyboardType="numeric"
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                onChangeText={(text)=>{
                  setUserObj({...userObj, identificationId: text})
                }}
              />
            </View> */}
            <View style={{ paddingTop: 10 }}>
              <Button
                title="บันทึก"
                color="#81E8B2"
                onPress={() => {
                   onRegister();
                }}
              />
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
     flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  emptyView: {
    paddingHorizontal: 10, 
    flexBasis: '25%', 
    backgroundColor: '#81E8B2'
  },
});