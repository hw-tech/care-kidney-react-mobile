import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, ScrollView, StatusBar, TouchableOpacity } from 'react-native';
import { useIsFocused } from "@react-navigation/native";

import { Text, View } from '../components/Themed';
import { ContentStyles, UtilsStyles } from '../constants/CustomStyles';

import { Ionicons } from '@expo/vector-icons';

import UserInfo from '../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { DataTable } from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';

import axios from 'axios';
import { ROOT_URL } from '../constants/Variable';

export default function UserAdminScreen({ route, navigation } : { route: any, navigation: any }) {
    const [userList, setUserList] = React.useState(Array());
    const isVisible = useIsFocused();
    const [spinner, setSpinner] = useState(false);

    useEffect(() => {
        if (isVisible) {
            getAllUsers();
        }
    }, [isVisible]);

    const getAllUsers = async () => {
        setSpinner(true);
        await axios.get(`${ROOT_URL}/users/all/Y`)
        .then(response => {
            if(response.status == 200){
                setUserList(response.data);
                setSpinner(false);
            }
        }, (error) => {
            console.log(error);
            setSpinner(false);
        });
    }

    return (
        <React.Fragment>
            <SafeAreaView style={ContentStyles.topSafeAreaView} />
            <SafeAreaView style={ContentStyles.maincontainer}>
                <StatusBar backgroundColor="#81E8B2" />
                <View style={ContentStyles.customNavBar}>
                    <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()}/>
                    <View style={styles.imgHeaderView}>
                        <Image
                        style={{ width: 40, height: 40, alignSelf: 'center' }}
                        source={require('./../assets/images/kidney-title.png')}
                        />
                    </View>
                    <View style={styles.emptyView}></View>
                </View>
                <View style={ContentStyles.menuImg}>
                    <Image
                        style={{ width: 75, height: 75 }}
                        source={require('./../assets/images/menu/menu-admin.png')}
                    />
                    <Text style={ContentStyles.titleText}>รายชื่อผู้ดูแลระบบ</Text>
                </View>
                <View style={ContentStyles.whiteBackgroundColor}>

                    <Spinner
                        visible={spinner}
                        textContent={'กรุณารอสักครู่ ...'}
                        textStyle={{color: '#FFF'}}
                    />

                    <DataTable style={{flex: 1}}>
                        {/* <DataTable.Header>
                            <DataTable.Title>ชื่อผู้ใช้งาน</DataTable.Title>
                        </DataTable.Header> */}
                        <ScrollView>
                        {
                            userList.map((item, key) => {
                            return(
                                    <TouchableOpacity key={key} onPress={() => {
                                        const jsonValue = JSON.stringify(item);
                                        AsyncStorage.setItem('userSelected', jsonValue);
                                        navigation.push('Profile.Admin', {item: "EDIT_ADMIN_USER"});
                                    }}>
                                        <DataTable.Row key={key}>
                                            <DataTable.Cell>{item.title_name} {item.firstname} {item.lastname}</DataTable.Cell>
                                        </DataTable.Row>
                                    </TouchableOpacity>
                                )
                            })
                        }
                        </ScrollView>
                    </DataTable>
                    <TouchableOpacity style={styles.floatingBtn} onPress={() => {
                        navigation.push('Profile.Admin', {item: "ADD_ADMIN_USER"});
                    }}>
                        <Text style={{ fontSize: 30, color: '#fff' }}>+</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        </React.Fragment>
    );
}

const styles = StyleSheet.create({
    backButtonView: {
      paddingLeft: 20,
      flexBasis: '25%'
    },
    imgHeaderView: {
      flexBasis: '50%',
      backgroundColor: '#81E8B2'
    },
    emptyView: {
      paddingHorizontal: 10, 
      flexBasis: '25%', 
      backgroundColor: '#81E8B2'
    },
    floatingBtn: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
        backgroundColor: "#81E8B2",
        width: 50,
        height: 50,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 10000
    }
});
  