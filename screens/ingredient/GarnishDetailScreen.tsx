import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList, ScrollView, StatusBar, TouchableOpacity, Alert, Button } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { Text, View } from '../../components/Themed';
import { Card, Title } from 'react-native-paper';
import { ContentStyles, UtilsStyles } from '../../constants/CustomStyles';
import { Ionicons } from '@expo/vector-icons';

import axios from 'axios';
import UserInfo from '../../data-model/UserInfo';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import { ROOT_URL } from '../../constants/Variable';
import ingredient from '../../data-model/ingredient';
import Spinner from 'react-native-loading-spinner-overlay';

export default function GarnishDetailScreen({ route, navigation }: { route: any, navigation: any }) {
    const [garnishDetail, setGarnishDetail] = React.useState(new ingredient());
    const numColumns = 2;
    const [spinner, setSpinner] = useState(false);
    const deleteData = async () => {
        setSpinner(true);
        await axios.post(`${ROOT_URL}/garnish/delete/${route.params.itemId}`)
            .then(response => {
                if (response.status == 200) {
                    Toast.show('ลบข้อมูลสำเร็จ', Toast.SHORT);
                    setSpinner(false);
                    navigation.replace('GarnishList');
                }
            });
    };
    // var userInfo: UserInfo;

    const [userInfo, setUserInfo] = React.useState(new UserInfo());
    useEffect(() => {
        getData().then(
            (data) => {
                setUserInfo(data);
                findGarnishByGarnishId();
            },
            (err) => {
                console.log(err);
            }
        );
    }, []);

    const getData = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('userInfo')
            return jsonValue != null ? JSON.parse(jsonValue) : null;
        } catch (e) {
            console.log(e);
        }
    }

    function renderButtonDelete() {
        if (userInfo.admin_flag !== 'N') {

            return (

                <TouchableOpacity style={{
                    position: 'absolute',
                    margin: 16,
                    right: 0,
                    bottom: 0,
                    backgroundColor: "#e82323",
                    width: 50,
                    height: 50,
                    borderRadius: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                    zIndex: 10000
                }}
                    onPress={() => {
                        deleteData();
                    }}
                >
                    <Ionicons name="ios-close-circle" size={25} style={{ marginBottom: 0, color: '#fff' }} />
                </TouchableOpacity>
            );
        }

    }
    const findGarnishByGarnishId = async () => {
        setSpinner(true);
        await axios.get(`${ROOT_URL}/garnish/${route.params.itemId}/`)
            .then(response => {
                if (response.status == 200) {
                    for (let _i = 0; _i < response.data.length; _i++) {
                        response.data[_i].key = _i;
                    }
                    setGarnishDetail(response.data[0]);
                    setSpinner(false);

                }
            }, (error) => {
                Toast.show(error, Toast.SHORT)
                setSpinner(false);
            });


    }
    function renderButtonEdit() {
        if (userInfo.admin_flag !== 'N') {
            return (
                <TouchableOpacity style={{
                    position: 'absolute',
                    margin: 16,
                    right: 0,
                    bottom: 70,
                    backgroundColor: "#81E8B2",
                    width: 50,
                    height: 50,
                    borderRadius: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                    zIndex: 10000
                }}
                    onPress={() => navigation.push('ManageGarnish', { itemId: route.params.itemId, })}
                >
                    <Ionicons name="ios-create" size={25} style={{ marginBottom: 4, color: '#fff' }} />
                </TouchableOpacity>
            );
        }
    }

    return (
        <React.Fragment>
            <SafeAreaView style={ContentStyles.topSafeAreaView} />
            <SafeAreaView style={ContentStyles.maincontainer}>
                <Spinner
                    visible={spinner}
                    textContent={'กรุณารอสักครู่ ...'}
                    textStyle={{ color: '#FFF' }}
                />
                <StatusBar backgroundColor="#81E8B2" />
                <View style={ContentStyles.customNavBar}>
                    <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
                    <View style={styles.imgHeaderView}>
                        <Image
                            style={{ width: 60, height: 60, alignSelf: 'center' }}
                            source={require('./../../assets/images/kidney-title.png')}
                        />
                    </View>
                    <View style={styles.emptyView}></View>
                </View>
                <View style={ContentStyles.menuImg}>
                    <Image
                        style={{ width: 75, height: 75 }}
                        source={require('./../../assets/images/menu/menu-incrediant.png')}
                    />
                    <Text style={ContentStyles.titleText}>เครื่องปรุง</Text>
                </View>
                {renderButtonEdit()}
                {renderButtonDelete()}
                <ScrollView
                    contentContainerStyle={{ flexGrow: 1 }}
                >
                    <View style={{ flex: 1, flexDirection: 'row', marginLeft: '13%', justifyContent: 'space-around' }}>
                        <View style={{ width: "90%", marginRight: 50, marginTop: 10, borderColor: "#000", borderBottomLeftRadius: 30, borderBottomRightRadius: 30, borderTopLeftRadius: 30, borderTopRightRadius: 30 }}>
                            <View style={{ flex: 0, justifyContent: 'space-between' }}>
                                <Card style={{ flex: 0, margin: 1, }} >
                                    <Card.Cover source={garnishDetail.seasoning_image_url == "" || garnishDetail.seasoning_image_url == null ? require('./../../assets/images/menu/menu-incrediant.png') : { uri: garnishDetail.seasoning_image_url }} style={{ height: 280, width: "auto", padding: 6, backgroundColor: '#FFF' }} />
                                </Card>
                                <Text style={{ fontSize: 25, color: "#ffbd59", fontWeight: "bold", alignSelf: "center", marginBottom: 5 }}>{garnishDetail.seasoning_name_th}</Text>
                                <Text style={{ fontSize: 25, color: "#ffbd59", fontWeight: "bold", alignSelf: "center", marginBottom: 5 }}>{garnishDetail.seasoning_name_en}</Text>
                            </View>
                            <View style={{ marginTop: 30, marginBottom: 20 }}>
                                <View style={{ flex: 0.8, flexDirection: 'column', justifyContent: 'center', alignSelf: "center", flexWrap: 'wrap' }}>
                                    <Text style={{ fontSize: 23, color: "#56c8cb", fontWeight: "bold", marginBottom: 5 }}>ปริมาณโซเดียมในเครื่องปรุง</Text>
                                </View>
                                <View style={ContentStyles.appButton}>
                                    <Text style={{ fontSize: 25, color: "#FFF", fontWeight: "700", alignSelf: "center" }}>{garnishDetail.sodium} mg </Text>
                                    <Text style={{ fontSize: 25, color: "#FFF", fontWeight: "700", alignSelf: "center" }}>ต่อ {garnishDetail.unit}</Text>
                                </View>
                                <View style={{ width: '100%', marginTop: 10, height: 20, borderColor: "#fff", borderBottomLeftRadius: 20, borderBottomRightRadius: 20, borderTopLeftRadius: 20, borderTopRightRadius: 20 }}></View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </React.Fragment>
    );
}

const styles = StyleSheet.create({
    backButtonView: {
        paddingLeft: 20,
        flexBasis: '25%'
    },
    imgHeaderView: {
        flexBasis: '50%',
        backgroundColor: '#81E8B2'
    },
    imgHeaderViewSx: {
        flexBasis: '25%',
        backgroundColor: '#81E8B2',
        paddingRight: 20
    },
    emptyView: {
        paddingHorizontal: 10,
        flexBasis: '25%',
        backgroundColor: '#81E8B2'
    },
});
