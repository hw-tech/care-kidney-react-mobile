import React, { useState, useEffect } from 'react';
import { StyleSheet, Button, TextInput, SafeAreaView, ScrollView, Picker, StatusBar, Image, YellowBox } from 'react-native';

import { Text, View } from '../../components/Themed';
import { ContentStyles, UtilsStyles } from '../../constants/CustomStyles';
import { Ionicons } from '@expo/vector-icons';

import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { ROOT_URL } from '../../constants/Variable';
import UserInfo from '../../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native';
import { Card } from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';
// 1. ให้ import firebase config

import { firebaseConfig } from '../../constants/firebase';
// 2. ให้ import กล้อง
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
// 3. import firebase
import * as firebase from 'firebase/app';
import 'firebase/storage';
import ingredient from '../../data-model/ingredient';

export default function ManageGarnishScreen({ route, navigation }: { route: any, navigation: any }) {
  const [userInfo, setUserInfo] = React.useState(new UserInfo());
  const [garnishDetail, setGarnishDetail] = React.useState(new ingredient());
  // 4. สร้างตัวแปรเอาไว้เก็บรูป
  const [imgUrl, setImgUrl] = React.useState();
  const [spinner, setSpinner] = useState(false);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log(e);
    }
  }

  useEffect(() => {
    // 13. สร้างตัวแปร check mounted
    let isMounted = true;
    getData().then(
      (data) => {
        // 14. check isMounted
        if (isMounted) {
          setUserInfo(data);
        }
        // 5. init firebase
        if (!firebase.apps.length) {
          firebase.initializeApp(firebaseConfig);
        }
        //6. ignore warning
        YellowBox.ignoreWarnings(['Setting a timer']);
      },
      (err) => {
        console.log(err);
      }
    );
    findGarnishByGarnishId();
    // 7. ขอ permission
    getPermissionAsync();
    // 15. set false
    return () => { isMounted = false };
  }, []);

  const findGarnishByGarnishId = async () => {
    setSpinner(true);
    await axios.get(`${ROOT_URL}/garnish/${route.params.itemId}/`)
      .then(response => {
        if (response.status == 200) {
          for (let _i = 0; _i < response.data.length; _i++) {
            response.data[_i].key = _i;
          }
          setGarnishDetail(response.data[0]);
          setSpinner(false);
          
        }
      }, (error) => {
        Toast.show(error, Toast.SHORT)
        setSpinner(false);
      });


  }
  // 8. func ขอ permission
  const getPermissionAsync = async () => {
    if (Constants.platform?.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  // 9. function เปิดรูป
  const onSelectPicture = async () => {
    ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1
    }).then((result) => {
      if (!result.cancelled) {
        // User picked an image
        console.log(result);

        const { height, width, type, uri } = result;
        return uriToBlob(uri);
      }
    }).then((blob) => {
      if (typeof (blob) !== 'undefined') {
        uploadToFirebase(blob);
      }
      return;
    }).then((snapshot) => {
      console.log("get file..");
    }).catch((error) => {
      throw error;
    });
  };
  // 10. เปลี่ยน uri เป็น blob
  const uriToBlob = (uri: any) => {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        // return the blob
        resolve(xhr.response);
      };

      xhr.onerror = function () {
        // something went wrong
        reject(new Error('uriToBlob failed'));
      };

      // this helps us get a blob
      xhr.responseType = 'blob';

      xhr.open('GET', uri, true);
      xhr.send(null);
    });
  }
  // 12. upload to firebase
  const uploadToFirebase = (blob: any) => {
    setSpinner(true);
    return new Promise((resolve, reject) => {
      var date = new Date().getDate();
      var storageRef = firebase.storage().ref();
      storageRef.child(`seasoning/${garnishDetail.seasoning_name_en}-${date}.jpg`).put(blob, {
        contentType: 'image/jpeg'
      }).then((snapshot) => {
        blob.close();

        // download url
        storageRef.child(`seasoning/${garnishDetail.seasoning_name_en}-${date}.jpg`).getDownloadURL().then((result) => {
          garnishDetail.seasoning_image_url = result;
          setImgUrl(result);

          setSpinner(false);
          resolve(snapshot);
        });

      }).catch((error) => {
        reject(error);
      });
    });
  }
  const onUpdate = async () => {

    garnishDetail.process = "ManagGarnishScreen";
    garnishDetail.updateUser = userInfo.firstname;
    if (!garnishDetail.seasoning_name_th || !garnishDetail.seasoning_name_en || !garnishDetail.sodium || !garnishDetail.unit || !garnishDetail.seasoning_id) {
      Toast.show('กรุณากรอกข้อมูลให้ครบถ้วน', Toast.SHORT);
      return;
    }
    setSpinner(true);
   await axios.put(`${ROOT_URL}/garnish/`, garnishDetail)
      .then(response => {
        if (response.status == 200) {
          Toast.show('แก้ไขข้อมูลสำเร็จ', Toast.SHORT);
          setSpinner(false);
          navigation.replace('GarnishDetail',{ itemId: garnishDetail.seasoning_id });
        }
      }, (error) => {
        setSpinner(false);
        console.log(error);

      });
  };


  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
      <Spinner
          visible={spinner}
          textContent={'กรุณารอสักครู่ ...'}
          textStyle={{ color: '#FFF' }}
        />
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 60, height: 60, alignSelf: 'center' }}
              source={require('./../../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75 }}
            source={require('./../../assets/images/ingredient.png')}
          />
          <Text style={ContentStyles.titleText}>แก้ไขเครื่องปรุง</Text>
        </View>
        <View style={ContentStyles.whiteBackgroundColor}>
          <ScrollView>
            <View style={{ flex: 0, justifyContent: 'space-between' }}>
              <Card style={{ flex: 0, margin: 30 }} onPress={() => {
                onSelectPicture();
              }}>
                <Card.Cover source={garnishDetail.seasoning_image_url == "" || garnishDetail.seasoning_image_url == null ? require('./../../assets/images/menu/menu-incrediant.png') : { uri: garnishDetail.seasoning_image_url }} style={{ height: 280, width: "auto", padding: 3 }} />
              </Card>
            </View>
            <Text>ชื่อเครื่องปรุง (ภาษาไทย)</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                value={garnishDetail.seasoning_name_th}
                onChangeText={(text) => {
                  setGarnishDetail({ ...garnishDetail, seasoning_name_th: text })

                }}
              />
            </View>
            <Text>ชื่อเครื่องปรุง (ภาษาอังกฤษ)</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                value={garnishDetail.seasoning_name_en}
                onChangeText={(text) => {
                  setGarnishDetail({ ...garnishDetail, seasoning_name_en: text })
                }}
              />
            </View>
            <Text>โซเดียม</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                value={garnishDetail.sodium}
                onChangeText={(text) => {
                  setGarnishDetail({ ...garnishDetail, sodium: text })
                }}
              />
            </View>
            <Text>หน่วย</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                value={garnishDetail.unit}
                onChangeText={(text) => {
                  setGarnishDetail({ ...garnishDetail, unit: text })
                }}
              />
            </View>




            <View style={{ paddingTop: 10 }}>
              <Button
                title="แก้ไข"
                color="#81E8B2"
                onPress={() => {
                  onUpdate();
                }}
              />
            </View>
          </ScrollView>
          <Spinner
            visible={spinner}
            textContent={'กรุณารอสักครู่ ...'}
            textStyle={{ color: '#FFF' }}
          />
        </View>
      </SafeAreaView>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  imgHeaderViewSx: {
    flexBasis: '25%',
    backgroundColor: '#81E8B2',
    paddingRight: 20
  },
  emptyView: {
    paddingHorizontal: 10,
    flexBasis: '25%',
    backgroundColor: '#81E8B2'
  },
});