import * as React from 'react';
import { StyleSheet, Image, SafeAreaView, ScrollView } from 'react-native';

import { Text, View } from '../../components/Themed';
import { Ionicons } from '@expo/vector-icons';
import { DataTable } from 'react-native-paper';

export default function IngredientDetailScreen({ navigation } : { navigation: any }) {
  var itemList = [  
    {key: 1, name: '1 ถ้วย', sodium: 250},
    {key: 2, name: '1 ช้อนโต๊ะ', sodium: 80},
    {key: 3, name: '10 กรัม', sodium: 10},
  ];

  return (
    <React.Fragment>
      <SafeAreaView style={{ flex: 0, backgroundColor: '#CE9C6F' }} />
      <View style={{ backgroundColor: '#CE9C6F' }}>
        <Ionicons name="ios-arrow-back" size={40} style={{ paddingLeft: 20 }} onPress={() => navigation.goBack()}/>
      </View>
      <SafeAreaView style={styles.container}>
        <View style={styles.space}>
          <Image
            style={styles.logo}
            source={require('./../../assets/images/ingredient.png')}
          />
          <Text style={styles.title_header}>เกลือ</Text>
        </View>
        <View style={styles.content}>
          <DataTable>
            <DataTable.Header>
              <DataTable.Title>หน่วยที่ใช้วัด</DataTable.Title>
              <DataTable.Title>ปริมาณโซเดียม</DataTable.Title>
            </DataTable.Header>
            <ScrollView>
              {
                itemList.map((item, key) => {
                  return(
                      <DataTable.Row key={key}>
                        <DataTable.Cell>{item.name}</DataTable.Cell>
                        <DataTable.Cell>{item.sodium}</DataTable.Cell>
                      </DataTable.Row>
                    )
                })
              }
            </ScrollView>
          </DataTable>
        </View>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CE9C6F'
  },
  space: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CE9C6F',
    margin: 10,
  },
  content: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingHorizontal: 30
  },
  title_header: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#FFF',
  },
  logo: {
    width: 75,
    height: 75,
  },
});
