import React, { PureComponent, useEffect,useState } from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList, ScrollView, StatusBar, TouchableOpacity, Alert, Button } from 'react-native';
import { SearchBar, Overlay } from 'react-native-elements';
import { Text, View } from '../../components/Themed';
import { Card, Title } from 'react-native-paper';
import { ContentStyles, UtilsStyles } from '../../constants/CustomStyles';
import { Ionicons } from '@expo/vector-icons';
import axios from 'axios';
import UserInfo from '../../data-model/UserInfo';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import { ROOT_URL } from '../../constants/Variable';
import Spinner from 'react-native-loading-spinner-overlay';
export default function GarnishListScreen({ navigation }: { navigation: any }) {
    const numColumns = 2;
    // var userInfo: UserInfo;

    const picsumImages = [
        { key: 1, name: 'เครื่องปรุง', pic: require('./../../assets/images/ingredient.png'), path: 'Consumption' },
        { key: 2, name: 'เครื่องปรุง', pic: require('./../../assets/images/ingredient.png'), path: 'Consumption' },
        { key: 3, name: 'เครื่องปรุง', pic: require('./../../assets/images/ingredient.png'), path: 'Consumption' },
        { key: 4, name: 'เครื่องปรุง', pic: require('./../../assets/images/ingredient.png'), path: 'Consumption' },
        { key: 5, name: 'เครื่องปรุง', pic: require('./../../assets/images/ingredient.png'), path: 'Consumption' },
        { key: 6, name: 'เครื่องปรุง', pic: require('./../../assets/images/ingredient.png'), path: 'Consumption' },
    ];

    const [images, setImages] = React.useState(picsumImages);
    const [userInfo, setUserInfo] = React.useState(new UserInfo());
    const [search, setSearch] = React.useState('')
    const [spinner, setSpinner] = useState(false);
    const [garnishObj, setgarnishObj] = useState([]);
    useEffect(() => {
        getData().then(
            (data) => {
                setUserInfo(data);
                getAllGarnish();
            },
            (err) => {
                console.log(err);
            }
        );
        
    }, []);

    const getAllGarnish = async () => {
        setSpinner(true);
        await axios.get(`${ROOT_URL}/garnish/`)
        .then(response => {
            if (response.status == 200) {
                setgarnishObj(response.data);
                setSpinner(false);
            }
        }, (error) => {
            Toast.show(error, Toast.SHORT)
            setSpinner(false);
        });
      }

    const getData = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('userInfo')
            return jsonValue != null ? JSON.parse(jsonValue) : null;
        } catch (e) {
            console.log(e);
        }
    }

    const confirmLogoutAlert = () => {
        Alert.alert(
            "แจ้งเตือน",
            "คณต้องการออกจากระบบหรือไม่ ?",
            [
                {
                    text: "ยกเลิก",
                    onPress: () => { },
                    style: "cancel"
                },
                {
                    text: "ตกลง",
                    onPress: () => {
                        AsyncStorage.removeItem('userInfo');
                        navigation.push('Login');
                    }
                }
            ],
            { cancelable: false }
        );
    }
    function renderButtonAdd() {
        if (userInfo.admin_flag !== 'N') {
            return (
                <TouchableOpacity style={{
                    position: 'absolute',
                    margin: 15,
                    right: 0,
                    bottom: 0,
                    backgroundColor: "#81E8B2",
                    width: 50,
                    height: 50,
                    borderRadius: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                    zIndex: 10000
                }}
                    onPress={() => navigation.push('AddGarnish')}
                >
                    <Text style={{ fontSize: 30, color: '#fff' }}>+</Text>
                </TouchableOpacity>
            );
        }
    }

    function renderItem({ item }: { item: any }) {
        return (
            // <Card style={{ flex: 1, margin: 10}} onPress={() => navigation.push(item.path)}>
            //   <Card.Content style={{height: 90}}>
            //     <Title style={{fontSize: 18}}>{item.name}</Title>
            //   </Card.Content>
            //   <Card.Cover source={item.pic} style={{ height: 150, width: "auto", padding: 25}}/>
            // </Card>
            <TouchableOpacity style={{ flex: 1, margin: 20, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }} onPress={() => navigation.push('GarnishDetail', {
                itemId: item.seasoning_id,
                itemNameTH: item.seasoning_name_th,
                itemNameEN: item.seasoning_name_en,
                itemSodium: item.sodium,
                itemUnit: item.unit,
                itemImg: item.seasoning_image_url

            })}>
                <Card.Cover source={item.seasoning_image_url == "" || item.seasoning_image_url == null ? require('./../../assets/images/menu/menu-incrediant.png') : { uri: item.seasoning_image_url }} style={{ height: 130, width: 130, padding: 3,marginTop: 3 }} />
                <View style={{ flex: 0.8, flexDirection: 'column', justifyContent: 'center',alignSelf: "center", flexWrap: 'wrap' }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#f5b43b', paddingTop: 10 }}>{item.seasoning_name_th}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    return (
        <React.Fragment>
           <SafeAreaView style={ContentStyles.topSafeAreaView} />
            <SafeAreaView style={ContentStyles.maincontainer}>
            <Spinner
                    visible={spinner}
                    textContent={'กรุณารอสักครู่ ...'}
                    textStyle={{ color: '#FFF' }}
                />
                <StatusBar backgroundColor="#81E8B2" />
                <View style={ContentStyles.customNavBar}>
                    <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
                    <View style={styles.imgHeaderView}>
                        <Image
                            style={{ width: 60, height: 60, alignSelf: 'center' }}
                            source={require('./../../assets/images/kidney-title.png')}
                        />
                    </View>
                    <View style={styles.emptyView}></View>
                </View>
                <View style={ContentStyles.menuImg}>
                    <Image
                        style={{ width: 75, height: 75 }}
                        source={require('./../../assets/images/menu/menu-incrediant.png')}
                    />
                    <Text style={ContentStyles.titleText}>เครื่องปรุง</Text>
                </View>
                {renderButtonAdd()}
                <View style={ContentStyles.contentAbsolute}>
                    <View style={{ padding: 10, backgroundColor: 'transparent' }}>
                        <FlatList data={garnishObj} renderItem={renderItem} numColumns={numColumns} />
                    </View>
                </View>
            </SafeAreaView>
            <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
        </React.Fragment>
    );
}


const styles = StyleSheet.create({
    backButtonView: {
        paddingLeft: 20,
        flexBasis: '25%'
    },
    imgHeaderView: {
        flexBasis: '50%',
        backgroundColor: '#81E8B2'
    },
    imgHeaderViewSx: {
        flexBasis: '25%',
        backgroundColor: '#81E8B2',
        paddingRight: 20
    },
    emptyView: {
        paddingHorizontal: 10,
        flexBasis: '25%',
        backgroundColor: '#81E8B2'
    },
});
