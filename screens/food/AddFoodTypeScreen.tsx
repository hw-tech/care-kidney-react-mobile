import React, { useState, useEffect } from 'react';
import { StyleSheet, Button, TextInput, SafeAreaView, ScrollView, Picker, StatusBar, Image ,YellowBox} from 'react-native';

import { Text, View } from '../../components/Themed';
import { ContentStyles, UtilsStyles } from '../../constants/CustomStyles';
import { Ionicons } from '@expo/vector-icons';

import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { ROOT_URL } from '../../constants/Variable';
import UserInfo from '../../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native';
import { Card } from 'react-native-paper';
// 1. import spinner
import Spinner from 'react-native-loading-spinner-overlay';

import { firebaseConfig } from '../../constants/firebase';

import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

import * as firebase from 'firebase/app';
import 'firebase/storage';


export default function AddFoodTypeScreen({ navigation }: { navigation: any }) {
  const [userInfo, setUserInfo] = React.useState(new UserInfo());
  const [imgUrl, setImgUrl] = React.useState();
  // 2. สร้างตัวแปรสำหรับ เปิด/ปิด spinner [ข้อ 3 อยู่บรรทัดที่ 98]
  const [spinner, setSpinner] = useState(false);
  useEffect(() => {
    let isMounted = true;
    getData().then(
      (data) => {
        if (isMounted){
        setUserInfo(data);
        }
        if (!firebase.apps.length) {
          firebase.initializeApp(firebaseConfig);
        }
        //6. ignore warning
        YellowBox.ignoreWarnings(['Setting a timer']);
      },
      (err) => {
        console.log(err);
      }
    );
    getPermissionAsync();
    return () => { isMounted = false };
  }, []);
  const getPermissionAsync = async () => {
    if (Constants.platform?.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  const onChangePicture = async () =>{
    ImagePicker.launchImageLibraryAsync({ 
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1
    }).then((result)=>{ 
      if (!result.cancelled) {
        // User picked an image
        // console.log(result);
        
        const {height, width, type, uri} = result;
        return uriToBlob(uri);
      }
    }).then((blob)=>{
      if(typeof(blob) !== 'undefined'){
        uploadToFirebase(blob);
      }
      return;
    }).then((snapshot)=>{
      console.log("get file..");
    }).catch((error)=>{
      throw error;
    }); 
  };


  const uriToBlob = (uri: any) => {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.onload = function () {
        // return the blob

        resolve(xhr.response);
      };

      xhr.onerror = function () {
        // something went wrong
        reject(new Error('uriToBlob failed'));
      };

      // this helps us get a blob
      xhr.responseType = 'blob';

      xhr.open('GET', uri, true);
      xhr.send(null);
    });
  }

  const uploadToFirebase = (blob: any) => {
    setSpinner(true);
    return new Promise((resolve, reject) => {
      var date = new Date().getDate();
      var storageRef = firebase.storage().ref();
      storageRef.child(`foodtype/foodtype-${date}.jpg`).put(blob, {
        contentType: 'image/jpeg'
      }).then((snapshot) => {
        blob.close();

        // download url
        storageRef.child(`foodtype/foodtype-${date}.jpg`).getDownloadURL().then((result) => {
          foodTypeObj.image_path = result;
          setImgUrl(result);
          
          
          setSpinner(false);
          resolve(snapshot);
        });

      }).catch((error) => {
        reject(error);
      });
    });
  }

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log(e);
    }
  }

  const [foodTypeObj, setFoodTypeObj] = useState({
    foodTypeNameTh: "",
    foodTypeNameEn: "",
    process: "",
    createUser: "",
    image_path:""
  });

  const onInsert = async () => {
    foodTypeObj.process = "AddFoodTypeScreen";
    foodTypeObj.createUser = userInfo.firstname;


    if (!foodTypeObj.foodTypeNameTh || !foodTypeObj.foodTypeNameEn) {
      Toast.show('กรุณากรอกข้อมูลให้ครบถ้วน', Toast.SHORT);
      return;
    }
 // 4. เปิด spinner ถ้ามีการเรียก service [อยู่ก่อน await axios]
 setSpinner(true);
    await axios.post(`${ROOT_URL}/foodtype/`, foodTypeObj)
      .then(response => {
        if (response.status == 200) {
          Toast.show('บันทึกข้อมูลสำเร็จ', Toast.SHORT);
          setSpinner(false);
          navigation.replace('FoodTypeList');
        }
      }, (error) => {
        console.log(error);
        setSpinner(false);
      });
  };

  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 60, height: 60, alignSelf: 'center' }}
              source={require('./../../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75 }}
            source={require('./../../assets/images/food.png')}
          />
          <Text style={ContentStyles.titleText}>เพิ่มประเภทอาหาร</Text>
        </View>
        <View style={ContentStyles.whiteBackgroundColor}>
           {/* 3. เอา spinner มาใส่ไว้ใน [whiteBackgroundColor] */}
           <Spinner
                        visible={spinner}
                        textContent={'กรุณารอสักครู่ ...'}
                        textStyle={{color: '#FFF'}}
                    />
          <ScrollView>
          <View style={{ flex: 0, justifyContent: 'space-between' }}>
              <Card style={{ flex: 0, margin: 30 }} onPress={() => {
                onChangePicture();
              }}>
                <Card.Cover source={imgUrl == "" || imgUrl == null ? require('./../../assets/images/ingredient.png') :  {uri: imgUrl}} style={{ height: 250, width: "auto", padding: 3 }} />
              </Card>
            </View>
            <Text>ชื่อประเภท (ภาษาไทย)</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                onChangeText={(text) => {
                  setFoodTypeObj({ ...foodTypeObj, foodTypeNameTh: text })
                }}

              />
            </View>
            <Text>ชื่อประเภท (ภาษาอังกฤษ)</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                onChangeText={(text) => {
                  setFoodTypeObj({ ...foodTypeObj, foodTypeNameEn: text })
                }}

              />
            </View>
            <View style={{ paddingTop: 10 }}>
              <Button
                title="บันทึก"
                color="#81E8B2"
                onPress={() => {
                  onInsert();
                }}
              />
            </View>
          </ScrollView>
          <Spinner
            visible={spinner}
            textContent={'กรุณารอสักครู่ ...'}
            textStyle={{color: '#FFF'}}
          />
        </View>
      </SafeAreaView>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  imgHeaderViewSx: {
    flexBasis: '25%',
    backgroundColor: '#81E8B2',
    paddingRight: 20
  },
  emptyView: {
    paddingHorizontal: 10,
    flexBasis: '25%',
    backgroundColor: '#81E8B2'
  },
  content: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingHorizontal: 30
  }
});