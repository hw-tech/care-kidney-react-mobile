import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';

import { Text, View } from '../../components/Themed';
import { Ionicons } from '@expo/vector-icons';
import { DataTable, Card, Title } from 'react-native-paper';
import FoodDetail from '../../data-model/FoodDetail';

import axios from 'axios';
import { ROOT_URL } from '../../constants/Variable';
import Toast from 'react-native-simple-toast';

import UserInfo from '../../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { ContentStyles } from '../../constants/CustomStyles';
import { StatusBar } from 'react-native';
import { color } from 'react-native-reanimated';
import FoodType from '../../data-model/FoodType';
import Spinner from 'react-native-loading-spinner-overlay';
export default function FoodDetailScreen({ route, navigation }: { route: any, navigation: any }) {
  const [foodDetail, setFoodDetail] = React.useState(new FoodDetail());
  const [userInfo, setUserInfo] = React.useState(new UserInfo());
  const [foodType, setFoodType] = React.useState(new FoodType());
  var itemList = new FoodDetail();
  var foodTypeName: FoodType = new FoodType();
  const [spinner, setSpinner] = useState(false);
  const [foodTypeObj, setFoodTypeObj] = useState({
    foodTypeName: ""
  });


  useEffect(() => {
    getData().then(
      (data) => {
        setUserInfo(data);
        findFoodByFoodId();

      },
      (err) => {
        console.log(err);
      }
    );
  }, []);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log(e);
    }
  }

  const findFoodByFoodId = async () => {
    setSpinner(true);
    await axios.get(`${ROOT_URL}/foods/${route.params.itemId}/`)
      .then(response => {
        if (response.status == 200) {
          setFoodDetail(response.data[0]);
          setSpinner(false);
        }
      }, (error) => {
        Toast.show(error, Toast.SHORT)
        setSpinner(false);
      });
  }

  const deleteData = async () => {
    axios.post(`${ROOT_URL}/foods/delete/${route.params.itemId}`)
      .then(response => {
        if (response.status == 200) {
          Toast.show('ลบข้อมูลสำเร็จ', Toast.SHORT);
          navigation.replace('FoodMenuTypeList');
        }
      });
  };

  function renderButtonDelete() {
    if (userInfo.admin_flag !== 'N') {
      console.log(itemList.food_id);

      return (

        <TouchableOpacity style={{
          position: 'absolute',
          margin: 16,
          right: 0,
          bottom: 0,
          backgroundColor: "#e82323",
          width: 50,
          height: 50,
          borderRadius: 100,
          justifyContent: 'center',
          alignItems: 'center',
          zIndex: 10000
        }}
          onPress={() => deleteData()}
        >
          <Ionicons name="ios-close-circle" size={25} style={{ marginBottom: 0, color: '#fff' }} />
        </TouchableOpacity>
      );
    }

  }

  function renderButtonEdit() {
    if (userInfo.admin_flag !== 'N') {
      return (
        <TouchableOpacity style={{
          position: 'absolute',
          margin: 16,
          right: 0,
          bottom: 70,
          backgroundColor: "#81E8B2",
          width: 50,
          height: 50,
          borderRadius: 100,
          justifyContent: 'center',
          alignItems: 'center',
          zIndex: 10000
        }}
          onPress={() => navigation.push('EditFoodDetailScreen', { itemId: route.params.itemId })}
        >
          <Ionicons name="ios-create" size={25} style={{ marginBottom: 4, color: '#fff' }} />
        </TouchableOpacity>
      );
    }
  }

  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 60, height: 60, alignSelf: 'center' }}
              source={require('./../../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75 }}
            source={require('./../../assets/images/menu/menu-food_type.png')}
          />
          <Text style={ContentStyles.titleText}>{foodDetail.food_name_th}</Text>
        </View>
        {renderButtonEdit()}
        {renderButtonDelete()}
        <Spinner
          visible={spinner}
          textContent={'กรุณารอสักครู่ ...'}
          textStyle={{ color: '#FFF' }}
        />
        <ScrollView
          contentContainerStyle={{ flexGrow: 1 }}
        >
          <View style={{ flex: 1, flexDirection: 'row', marginLeft: '13%', justifyContent: 'space-around' }}>
            <View style={{ width: "90%", marginRight: 50, marginTop: 10, borderColor: "#000", borderBottomLeftRadius: 30, borderBottomRightRadius: 30, borderTopLeftRadius: 30, borderTopRightRadius: 30 }}>
              <View style={{ flex: 0, justifyContent: 'space-between' }}>
                <Card style={{ flex: 0, margin: 1, }} >
                  <Card.Cover source={foodDetail.image_path == "" || foodDetail.image_path == null ? require('./../../assets/images/food.png') : { uri: foodDetail.image_path }} style={{ height: 280, width: "auto", padding: 6, backgroundColor: '#FFF' }} />
                </Card>
                <Text style={{ fontSize: 25, color: "#ffbd59", fontWeight: "bold", alignSelf: "center", marginBottom: 5 }}>{foodDetail.food_name_th}</Text>
                <Text style={{ fontSize: 25, color: "#ffbd59", fontWeight: "bold", alignSelf: "center", marginBottom: 5 }}>{foodDetail.food_name_en}</Text>
              </View>
              <View style={{ marginTop: 30, marginBottom: 20 }}>

                <Text style={{ fontSize: 24, color: "#56c8cb", fontWeight: "bold", alignSelf: "center", marginBottom: 10 }}>ปริมาณโซเดียมในอาหาร</Text>
                <Text style={{ fontSize: 10, color: "#56c8cb", fontWeight: "bold", alignSelf: "center", marginBottom: 10 }}></Text>
                <View style={ContentStyles.appButton}>
                  <Text style={{ fontSize: 25, color: "#FFF", fontWeight: "700", alignSelf: "center" }}>{foodDetail.sodium} mg </Text>
                  <Text style={{ fontSize: 23, color: "#FFF", fontWeight: "700", alignSelf: "center" }}>ต่อ {foodDetail.unit}</Text>
                </View>
                <View style={{ width: '100%', marginTop: 10, height: 20, borderColor: "#fff", borderBottomLeftRadius: 20, borderBottomRightRadius: 20, borderTopLeftRadius: 20, borderTopRightRadius: 20 }}></View>
              </View>
            </View>
          </View>
        </ScrollView>

      </SafeAreaView>
      <View></View>
      <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  imgHeaderViewSx: {
    flexBasis: '25%',
    backgroundColor: '#81E8B2',
    paddingRight: 20
  },
  emptyView: {
    paddingHorizontal: 10,
    flexBasis: '25%',
    backgroundColor: '#81E8B2'
  },
  content: {
    flex: 0,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingBottom: 50,
    paddingHorizontal: 30
  },
  contentTop: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    paddingTop: 10,
    paddingHorizontal: 30
  }
});


