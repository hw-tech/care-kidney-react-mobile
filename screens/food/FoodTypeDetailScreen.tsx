import React, { PureComponent, useEffect, useState } from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList, ScrollView, StatusBar, TouchableOpacity, Alert, Button } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { Text, View } from '../../components/Themed';
import { Card, Title } from 'react-native-paper';
import { ContentStyles, UtilsStyles } from '../../constants/CustomStyles';
import { Ionicons } from '@expo/vector-icons';
import axios from 'axios';
import UserInfo from '../../data-model/UserInfo';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import { ROOT_URL } from '../../constants/Variable';
import Spinner from 'react-native-loading-spinner-overlay';
export default function FoodTypeDetailScreen({ route, navigation }: { route: any, navigation: any }) {
    const [foodTypeObj, setFoodTypeObj] = useState(Array());
    const [spinner, setSpinner] = useState(false);
    const [foodTypeObj1, setFoodTypeObj1] = useState({
      foodTypeNameTh: "",
      foodTypeNameEn: "",
      process: "",
      updateUser: "",
      itemID:"",
      foodTypeID:""
    });

    const [userInfo, setUserInfo] = React.useState(new UserInfo());
    useEffect(() => {
      getData().then(
        (data) => {
          setUserInfo(data);
        },
        (err) => {
          console.log(err);
        }
      );
    }, []);
  
    const getData = async () => {
      try {
        const jsonValue = await AsyncStorage.getItem('userInfo')
        return jsonValue != null ? JSON.parse(jsonValue) : null;
      } catch (e) {
        console.log(e);
      }
    }

    function renderButtonDelete() {
        if (userInfo.admin_flag !== 'N') {
          
          return (
    
            <TouchableOpacity style={{
              position: 'absolute',
              margin: 16,
              right: 0,
              bottom: 0,
              backgroundColor: "#e82323",
              width: 50,
              height: 50,
              borderRadius: 100,
              justifyContent: 'center',
              alignItems: 'center',
              zIndex: 10000
            }}
              onPress={() => {
                deleteData();
              }}
            >
              <Ionicons name="ios-close-circle" size={25} style={{ marginBottom: 0, color: '#fff' }} />
            </TouchableOpacity>
          );
        }
    
      }

      const deleteData = async () => {
        foodTypeObj1.foodTypeID = route.params.itemId;
        axios.post(`${ROOT_URL}/foodtype/delete/${route.params.itemId}`, foodTypeObj1)
        .then(response => {
        if(response.status == 200){
            Toast.show('ลบข้อมูลสำเร็จ', Toast.SHORT);
            navigation.replace('FoodTypeList');
        }
      });
    };
    
      function renderButtonEdit() {
        if (userInfo.admin_flag !== 'N') {
          return (
            <TouchableOpacity style={{
              position: 'absolute',
              margin: 16,
              right: 0,
              bottom: 70,
              backgroundColor: "#81E8B2",
              width: 50,
              height: 50,
              borderRadius: 100,
              justifyContent: 'center',
              alignItems: 'center',
              zIndex: 10000
            }}
              onPress={() => navigation.push('ManageFoodType',{
                itemId: route.params.itemId,
                itemNameTH: route.params.itemNameTH,
                itemNameEN: route.params.itemNameEN,
                itemImg: route.params.itemImg

            })}
            >
              <Ionicons name="ios-create" size={25} style={{ marginBottom: 4, color: '#fff' }} />
            </TouchableOpacity>
          );
        }
      }

    return (
        <React.Fragment>
        <SafeAreaView style={ContentStyles.topSafeAreaView} />
              <SafeAreaView style={ContentStyles.maincontainer}>
              <Spinner
          visible={spinner}
          textContent={'กรุณารอสักครู่ ...'}
          textStyle={{ color: '#FFF' }}
        />
                  <StatusBar backgroundColor="#81E8B2" />
                  <View style={ContentStyles.customNavBar}>
                      <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
                      <View style={styles.imgHeaderView}>
                          <Image
                              style={{ width: 60, height: 60, alignSelf: 'center' }}
                              source={require('./../../assets/images/kidney-title.png')}
                          />
                      </View>
                      <View style={styles.emptyView}></View>
                  </View>
                  <View style={ContentStyles.menuImg}>
                      <Image
                          style={{ width: 75, height: 75 }}
                          source={require('./../../assets/images/food.png')}
                      />
                      <Text style={ContentStyles.titleText}>ประเภทอาหาร</Text>
                  </View>
          {renderButtonEdit()}
          {renderButtonDelete()}
          <View style={styles.contentTop}>
            <View style={{ flex: 0, justifyContent: 'space-between' }}>
              <Card style={{ flex: 0, margin: 30 }} >
                <Card.Cover source={{ uri: route.params.itemImg}} style={{ height: 250, width: "auto", padding: 6 ,backgroundColor: '#FFF'}} />
              </Card>
            </View>
          <Text style={{ paddingHorizontal: 20, alignSelf: 'center', fontSize: 28, fontWeight: 'bold', color: '#f5b43b', margin: 5, }}>{ route.params.itemNameTH }</Text>
          <Text style={{ paddingHorizontal: 20, alignSelf: 'center', fontSize: 28, fontWeight: 'bold', color: '#f5b43b', margin: 5, }}>({ route.params.itemNameEN })</Text>
          </View>
        </SafeAreaView>
        <View></View>
        <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
      </React.Fragment>
    );
}

const styles = StyleSheet.create({
    backButtonView: {
        paddingLeft: 20,
        flexBasis: '25%'
    },
    imgHeaderView: {
        flexBasis: '50%',
        backgroundColor: '#81E8B2'
    },
    imgHeaderViewSx: {
        flexBasis: '25%',
        backgroundColor: '#81E8B2',
        paddingRight: 20
    },
    emptyView: {
        paddingHorizontal: 10,
        flexBasis: '25%',
        backgroundColor: '#81E8B2'
    },
    content: {
      flex: 0,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingTop: 30,
      paddingBottom: 50,
      paddingHorizontal: 30
    },
    contentTop: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      borderBottomLeftRadius: 30,
      borderBottomRightRadius: 30,
      paddingTop: 10,
      paddingHorizontal: 30
    }
  });
