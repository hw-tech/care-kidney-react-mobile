import React, { useState, useEffect } from 'react';
import { StyleSheet, Button, TextInput, SafeAreaView, ScrollView, Picker, StatusBar, Image } from 'react-native';

import { Text, View } from '../../components/Themed';
import { ContentStyles, UtilsStyles } from '../../constants/CustomStyles';
import { Ionicons } from '@expo/vector-icons';
import { Card, Title } from 'react-native-paper';

import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { ROOT_URL } from '../../constants/Variable';
import UserInfo from '../../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
// 1. ให้ import firebase config
import { firebaseConfig } from '../../constants/firebase';

// 2. ให้ import กล้อง
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

// 3. import firebase
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { YellowBox } from 'react-native';

export default function CreateFoodDetailScreen({ navigation }: { navigation: any }) {
  const [userInfo, setUserInfo] = React.useState(new UserInfo());
  const [foodTypeList, setFoodTypeList] = React.useState(Array());
  const [spinner, setSpinner] = useState(false);
  // 4. สร้างตัวแปรเอาไว้เก็บรูป
  const [imgUrl, setImgUrl] = React.useState();
  useEffect(() => {
    getData().then(
      (data) => {
        setUserInfo(data);
        findFoodType();

        // 5. init firebase
        if (!firebase.apps.length) {
          firebase.initializeApp(firebaseConfig);
        }
        // 6. ignore warning
        YellowBox.ignoreWarnings(['Setting a timer']);
      },
      (err) => {
        console.log(err);
      }
    );
    // 7. ขอ permission
    getPermissionAsync();
  }, []);


  // 8. func ขอ permission
  const getPermissionAsync = async () => {
    if (Constants.platform?.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log(e);
    }
  }

  const [foodObj, setFoodObj] = useState({
    foodNameTh: "",
    foodNameEn: "",
    sodium: "",
    unit: "",
    foodTypeId: "",
    foodType: "",
    foodTypeName: "",
    process: "",
    createUser: "",
    image_path: ""
  });

  // 9. function เปิดรูป
  const onSelectPicture = async () => {
    ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1
    }).then((result) => {
      if (!result.cancelled) {
        // User picked an image
        // console.log(result);

        const { height, width, type, uri } = result;
        return uriToBlob(uri);
      }
    }).then((blob) => {
      if (typeof (blob) !== 'undefined') {
        uploadToFirebase(blob);
      }
      return;
    }).then((snapshot) => {
      console.log("get file..");
    }).catch((error) => {
      throw error;
    });
  };

  // 10. เปลี่ยน uri เป็น blob
  const uriToBlob = (uri: any) => {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        // return the blob
        resolve(xhr.response);
      };

      xhr.onerror = function () {
        // something went wrong
        reject(new Error('uriToBlob failed'));
      };

      // this helps us get a blob
      xhr.responseType = 'blob';

      xhr.open('GET', uri, true);
      xhr.send(null);
    });
  }

  // 12. upload to firebase
  const uploadToFirebase = (blob: any) => {
    setSpinner(true);
    return new Promise((resolve, reject) => {
      var storageRef = firebase.storage().ref();
      storageRef.child(`foods/${foodObj.foodNameEn}.jpg`).put(blob, {
        contentType: 'image/jpeg'
      }).then((snapshot) => {
        blob.close();

        // download url
        storageRef.child(`foods/${foodObj.foodNameEn}.jpg`).getDownloadURL().then((result) => {
          foodObj.image_path = result;
          setImgUrl(result);
          setSpinner(false);
          resolve(snapshot);
        });

      }).catch((error) => {
        reject(error);
      });
    });
  }

  const onInsert = async () => {
    foodObj.process = "CreateFoodDetailScreen";
    foodObj.createUser = userInfo.firstname;

    if (!foodObj.foodNameTh || !foodObj.foodNameEn || !foodObj.sodium || !foodObj.unit || !foodObj.foodType) {
      Toast.show('กรุณากรอกข้อมูลให้ครบถ้วน', Toast.SHORT);
      return;
    }

    // upload when save
    // 11. อัพโหลดรูปไปยัง firebase

    // console.log(res.data);
    setSpinner(true);
    await axios.post(`${ROOT_URL}/foods/`, foodObj)
      .then(response => {
        if (response.status == 200) {
          Toast.show('บันทึกข้อมูลสำเร็จ', Toast.SHORT);
          setSpinner(false);
          navigation.replace('FoodInfo.Type', { itemId: foodObj.foodTypeId, itemName: foodObj.foodTypeName });
        }
      }, (error) => {
        console.log(error);
        setSpinner(false);
      });
  };

  const findFoodType = async () => {
    setSpinner(true);
    await axios.get(`${ROOT_URL}/foodtype/`)
      .then(response => {
        if (response.status == 200) {
          const jsonValue = JSON.stringify(response.data);
          setFoodTypeList(response.data);
          setSpinner(false);
        }
      }, (error) => {
        console.log(error);
        setSpinner(false);
      });
  }



  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 60, height: 60, alignSelf: 'center' }}
              source={require('./../../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75 }}
            source={require('./../../assets/images/menu/menu-food_type.png')}
          />
          <Text style={ContentStyles.titleText}>เพิ่มข้อมูลอาหาร</Text>
        </View>
        <View style={styles.content}>
          <Spinner
            visible={spinner}
            textContent={'กรุณารอสักครู่ ...'}
            textStyle={{ color: '#FFF' }}
          />
          <ScrollView>
            <TouchableOpacity onPress={() => { onSelectPicture() }} >
              <View style={{ flex: 0, justifyContent: 'space-between' }}>
                <Card style={{ flex: 0, margin: 30 }} >
                  <Card.Cover style={{ height: 250, width: "auto", padding: 6 }} source={foodObj.image_path == "" || foodObj.image_path == null ? require('./../../assets/images/food.png') : { uri: foodObj.image_path }} />
                </Card>
              </View>
            </TouchableOpacity>
            <Text>ชื่อภาษาไทย</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                onChangeText={(text) => {
                  setFoodObj({ ...foodObj, foodNameTh: text })
                }}
              />
            </View>
            <Text>ชื่อภาษาอังกฤษ</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                onChangeText={(text) => {
                  setFoodObj({ ...foodObj, foodNameEn: text })
                }}
              />
            </View>
            <Text>จำนวนโซเดียม</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                placeholder="(มิลลิกรัม)"
                autoCapitalize="none"
                onChangeText={(text) => {
                  if (text === '') {
                    setFoodObj({ ...foodObj, sodium: '' })
                  } else if (/^\d+\.?\d*$/.test(text)) {
                    setFoodObj({ ...foodObj, sodium: text })
                  }
                }}
              />
            </View>
            <Text>หน่วย</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                placeholder="(เช่น 1 จาน)"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                onChangeText={(text) => {
                  setFoodObj({ ...foodObj, unit: text })
                }}
              />
            </View>
            <Text>ประเภทอาหาร</Text>
            <View style={UtilsStyles.actionPicker}>
              <Picker
                selectedValue={foodObj.foodType}
                style={{}}
                onValueChange={(itemValue, itemIndex) => {
                  setFoodObj({ ...foodObj, foodType: itemValue, foodTypeId: itemValue.food_type_id, foodTypeName: itemValue.food_type_name_th });
                }} >
                <Picker.Item label="กรุณาเลือก..." value="" />
                {
                  foodTypeList.map((item, key) => {
                    return (
                      <Picker.Item key={key} label={item.food_type_name_th} value={item} />
                    )
                  })
                }
                {/* <Picker.Item label="นาง" value="นาง" />
                    <Picker.Item label="นางสาว" value="นางสาว" /> */}
              </Picker>
            </View>
            <View style={{ paddingTop: 10, paddingBottom: 20 }}>
              <Button
                title="บันทึก"
                color="#81E8B2"
                onPress={() => {
                  onInsert();
                }}
              />
            </View>
            <Spinner
              visible={spinner}
              textContent={'กรุณารอสักครู่ ...'}
              textStyle={{ color: '#FFF' }}
            />
          </ScrollView>
        </View>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  imgHeaderViewSx: {
    flexBasis: '25%',
    backgroundColor: '#81E8B2',
    paddingRight: 20
  },
  emptyView: {
    paddingHorizontal: 10,
    flexBasis: '25%',
    backgroundColor: '#81E8B2'
  },
  content: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingHorizontal: 30
  }
});
