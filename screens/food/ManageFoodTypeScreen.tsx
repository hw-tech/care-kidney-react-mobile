import React, { useState, useEffect } from 'react';
import { StyleSheet, Button, TextInput, SafeAreaView, ScrollView, Picker, StatusBar, Image ,YellowBox} from 'react-native';

import { Text, View } from '../../components/Themed';
import { ContentStyles, UtilsStyles } from '../../constants/CustomStyles';
import { Ionicons } from '@expo/vector-icons';
import { Card, Title } from 'react-native-paper';
import FoodType from '../../data-model/FoodType';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { ROOT_URL } from '../../constants/Variable';
import UserInfo from '../../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
// 1. ให้ import firebase config
import { firebaseConfig } from '../../constants/firebase';
// 2. ให้ import กล้อง
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
// 3. import firebase
import * as firebase from 'firebase/app';
import 'firebase/storage';

export default function EditFoodDetailScreen({ route, navigation }: { route: any, navigation: any }) {
  const [userInfo, setUserInfo] = React.useState(new UserInfo());
  const [foodTypeList, setFoodTypeList] = React.useState(Array());
  const [foodTypeDetail, setFoodTypeDetail] = React.useState(new FoodType());
// 4. สร้างตัวแปรเอาไว้เก็บรูป
  const [imgUrl, setImgUrl] = React.useState();
  const [spinner, setSpinner] = useState(false);
  useEffect(() => {
    // 13. สร้างตัวแปร check mounted
    let isMounted = true;
    getData().then(
      (data) => {
        // 14. check isMounted
        if (isMounted){
        setUserInfo(data);
        
        }
        // 5. init firebase
        if (!firebase.apps.length) {
          firebase.initializeApp(firebaseConfig);
        }
        //6. ignore warning
        YellowBox.ignoreWarnings(['Setting a timer']);
      },
      (err) => {
        console.log(err);
      }
    );
    findFoodTypeByFoodId();
    // 7. ขอ permission
    getPermissionAsync();
    // 15. set false
    return () => { isMounted = false };
  }, []);

  const findFoodTypeByFoodId = async () => {
    await axios.get(`${ROOT_URL}/foodType/${route.params.itemId}/`)
      .then(response => {
        if (response.status == 200) {
          for (let _i = 0; _i < response.data.length; _i++) {
            response.data[_i].key = _i;
          }
          console.log(response.data[0]);
          
          setFoodTypeDetail(response.data[0]);
          console.log(foodTypeDetail);
        }
      }, (error) => {
        Toast.show(error, Toast.SHORT)
      });

    
  }
   // 8. func ขอ permission
  const getPermissionAsync = async () => {
    if (Constants.platform?.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  // 9. function เปิดรูป
  const onSelectPicture = async () => {
    ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1
    }).then((result) => {
      if (!result.cancelled) {
        // User picked an image
        console.log(result);

        const { height, width, type, uri } = result;
        return uriToBlob(uri);
      }
    }).then((blob) => {
      if (typeof (blob) !== 'undefined') {
        uploadToFirebase(blob);
      }
      return;
    }).then((snapshot) => {
      console.log("get file..");
    }).catch((error) => {
      throw error;
    });
  };

// 10. เปลี่ยน uri เป็น blob
  const uriToBlob = (uri: any) => {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.onload = function () {
        // return the blob

        resolve(xhr.response);
      };

      xhr.onerror = function () {
        // something went wrong
        reject(new Error('uriToBlob failed'));
      };

      // this helps us get a blob
      xhr.responseType = 'blob';

      xhr.open('GET', uri, true);
      xhr.send(null);
    });
  }
// 12. upload to firebase
  const uploadToFirebase = (blob: any) => {
    setSpinner(true);
    return new Promise((resolve, reject) => {
      var date = new Date().getDate();
      var storageRef = firebase.storage().ref();
      storageRef.child(`foodtype/foodtype-${date}.jpg`).put(blob, {
        contentType: 'image/jpeg'
      }).then((snapshot) => {
        blob.close();

        // download url
        storageRef.child(`foodtype/foodtype-${date}.jpg`).getDownloadURL().then((result) => {
          foodTypeDetail.image_path = result;
          setImgUrl(result);
          
          setSpinner(false);
          resolve(snapshot);
        });

      }).catch((error) => {
        reject(error);
      });
    });
  }

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log(e);
    }
  }




  const onUpdate = async () => {
    foodTypeDetail.process = "EditFoodTypeScreen";
    foodTypeDetail.updateUser = userInfo.firstname;
    foodTypeDetail.food_type_id = route.params.itemId;

    if (!foodTypeDetail.food_type_name_th) {
      foodTypeDetail.food_type_name_th = route.params.itemNameTH;
    }
    if (!foodTypeDetail.food_type_name_en) {
      foodTypeDetail.food_type_name_th = route.params.itemNameEN;
    }
    if(!foodTypeDetail.image_path){
      foodTypeDetail.image_path = route.params.itemImg;
    }
    setSpinner(true);
   await axios.put(`${ROOT_URL}/foodtype/`, foodTypeDetail)
      .then(response => {
        if (response.status == 200) {
          Toast.show('แก้ไขข้อมูลสำเร็จ', Toast.SHORT);
          setSpinner(false);
          navigation.replace('FoodTypeList');
        }
      }, (error) => {
        console.log(error);
        setSpinner(false);
      });
  };


  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 60, height: 60, alignSelf: 'center' }}
              source={require('./../../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75 }}
            source={require('./../../assets/images/food.png')}
          />
          <Text style={ContentStyles.titleText}>แก้ไขประเภทอาหาร</Text>
        </View>
        <View style={styles.content}>
        <Spinner
          visible={spinner}
          textContent={'กรุณารอสักครู่ ...'}
          textStyle={{ color: '#FFF' }}
        />
          <ScrollView>
            <View style={{ flex: 0, justifyContent: 'space-between' }}>
              <Card style={{ flex: 0, margin: 30 }} onPress={() => {
               onSelectPicture();
              }}>
                <Card.Cover source={imgUrl == "" || imgUrl == null ? {uri: route.params.itemImg} :  {uri: imgUrl}} style={{ height: 250, width: "auto", padding: 3 }} />
              </Card>
            </View>
            <Text>ชื่อประเภทภาษาไทย</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
               value = {foodTypeDetail.food_type_name_th}
                onChangeText={(text) => {
                  setFoodTypeDetail({ ...foodTypeDetail, food_type_name_th : text })
                }}
              />
            </View>
            <Text>ชื่อประเภทภาษาอังกฤษ</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                value = {foodTypeDetail.food_type_name_en}
                onChangeText={(text) => {
                  setFoodTypeDetail({ ...foodTypeDetail, food_type_name_en  : text })
                }}
              />
            </View>
            <View style={{ paddingTop: 10, paddingBottom: 20 }}>
              <Button
                title="แก้ไข"
                color="#81E8B2"
                onPress={() => {
                  onUpdate();
                }}
              />
            </View>
          </ScrollView>
          <Spinner
            visible={spinner}
            textContent={'กรุณารอสักครู่ ...'}
            textStyle={{color: '#FFF'}}
          />
        </View>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  imgHeaderViewSx: {
    flexBasis: '25%',
    backgroundColor: '#81E8B2',
    paddingRight: 20
  },
  emptyView: {
    paddingHorizontal: 10,
    flexBasis: '25%',
    backgroundColor: '#81E8B2'
  },
  content: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingHorizontal: 30
  }
});