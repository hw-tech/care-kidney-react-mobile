import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList, TouchableOpacity } from 'react-native';

import { Text, View } from '../../components/Themed';
import { Ionicons } from '@expo/vector-icons';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { ROOT_URL } from '../../constants/Variable';
import UserInfo from '../../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { ContentStyles, UtilsStyles } from '../../constants/CustomStyles';
import { StatusBar } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
export default function FoodTypeScreen({ route, navigation }: { route: any, navigation: any }) {

  const numColumns = 1;
  const [foodDetailList, setFoodDetailList] = React.useState(Array());
  const [userInfo, setUserInfo] = React.useState(new UserInfo());
  const [spinner, setSpinner] = useState(false);

  useEffect(() => {
    getData().then(
      (data) => {
        setUserInfo(data);
        findFoodByFoodTypeId();
      },
      (err) => {
        console.log(err);
      }
    );
  }, []);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log(e);
    }
  }

  const findFoodByFoodTypeId = async () => {
    setSpinner(true);
    await axios.get(`${ROOT_URL}/foods/foodType/${route.params.itemId}/`)
      .then(response => {
        if (response.status == 200) {
          for (let _i = 0; _i < response.data.length; _i++) {
            response.data[_i].key = _i;
          }
          setFoodDetailList(response.data);
          setSpinner(false);
        }
      }, (error) => {
        Toast.show(error, Toast.SHORT)
        setSpinner(false);
      });
  }

  function renderButtonAdd() {
    if (userInfo.admin_flag !== 'N') {
      return (
        <TouchableOpacity style={{
          position: 'absolute',
          margin: 15,
          right: 0,
          bottom: 0,
          backgroundColor: "#81E8B2",
          width: 50,
          height: 50,
          borderRadius: 100,
          justifyContent: 'center',
          alignItems: 'center',
          zIndex: 10000
        }}
          onPress={() => navigation.push('CreateFoodDetail')}
        >
          <Text style={{ fontSize: 30, color: '#fff' }}>+</Text>
        </TouchableOpacity>
      );
    }

  }

  function renderItem({ item }: { item: any }) {
    return (
      <TouchableOpacity onPress={() => { navigation.push('FoodInfo.Detail', { itemId: item.food_id }) }} >
        <View style={{ height: 50, flexDirection: 'row', marginBottom: 10, borderBottomWidth: 0.5 }}>
          <Image
            style={{ height: 45, width: 50, borderRadius: 5 }}
            source={item.image_path == "" || item.image_path == null ? require('./../../assets/images/food.png') : { uri: item.image_path }}
          />
          <Text style={{ paddingHorizontal: 20, alignSelf: 'center', fontSize: 18 }}>{`${item.food_name_th} (โซเดียม ${item.sodium})`}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        <Spinner
          visible={spinner}
          textContent={'กรุณารอสักครู่ ...'}
          textStyle={{ color: '#FFF' }}
        />
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 60, height: 60, alignSelf: 'center' }}
              source={require('./../../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75 }}
            source={require('./../../assets/images/menu/menu-food_type.png')}
          />
          <Text style={ContentStyles.titleText}>{route.params.itemName}</Text>
        </View>
        {renderButtonAdd()}
        <View style={styles.content}>
          <FlatList data={foodDetailList} renderItem={renderItem} numColumns={numColumns} />
        </View>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  imgHeaderViewSx: {
    flexBasis: '25%',
    backgroundColor: '#81E8B2',
    paddingRight: 20
  },
  emptyView: {
    paddingHorizontal: 10,
    flexBasis: '25%',
    backgroundColor: '#81E8B2'
  },
  content: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingHorizontal: 30
  }
});