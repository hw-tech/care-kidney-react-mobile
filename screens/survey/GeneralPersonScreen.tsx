import * as React from 'react';
import { StyleSheet, Image, SafeAreaView, ScrollView, Button } from 'react-native';

import { Text, View } from '../../components/Themed';
import { Ionicons } from '@expo/vector-icons';
import { RadioButton } from 'react-native-paper';

export default function GeneralPersonScreen({ navigation } : { navigation: any }) {

  const [checked1, setChecked1] = React.useState('1');
  const [checked2, setChecked2] = React.useState('1');
  const [checked3, setChecked3] = React.useState('1');
  const [checked4, setChecked4] = React.useState('1');
  const [checked5, setChecked5] = React.useState('1');

  return (
    <React.Fragment>
      <SafeAreaView style={{ flex: 0, backgroundColor: '#CE9C6F' }} />
      <View style={{ backgroundColor: '#CE9C6F' }}>
        <Ionicons name="ios-arrow-back" size={40} style={{ paddingLeft: 20 }} onPress={() => navigation.goBack()}/>
      </View>
      <SafeAreaView style={styles.container}>
        <View style={styles.space}>
          <Image
            style={styles.logo}
            source={require('./../../assets/images/menu/menu-servey.png')}
          />
          <Text style={styles.title_header}>ประเมินสุขภาพไต</Text>
        </View>
        <View style={styles.content}>
          <View style={{paddingVertical: 10}}>
            <Text style={styles.label_txt}>1. ท่านชอบทานอาหารรสจัดหรือไม่</Text>
            <View style={{flexDirection: 'row'}}>
              <RadioButton
                value="1"
                status={ checked1 === '1' ? 'checked' : 'unchecked' }
                onPress={() => setChecked1('1')}
              />
              <Text style={styles.radio_txt}>ใช่</Text>
              <RadioButton
                value="2"
                status={ checked1 === '2' ? 'checked' : 'unchecked' }
                onPress={() => setChecked1('2')}
              />
              <Text style={styles.radio_txt}>ไม่ใช่</Text>
            </View>
          </View>
          <View style={{paddingVertical: 10}}>
            <Text style={styles.label_txt}>2. การดื่มแอลกอฮอล์ต่อ 1 อาทิตย์</Text>
            <View style={{flexDirection: 'row'}}>
              <RadioButton
                value="1"
                status={ checked2 === '1' ? 'checked' : 'unchecked' }
                onPress={() => setChecked2('1')}
              />
              <Text style={styles.radio_txt}>ไม่ดื่ม</Text>
              <RadioButton
                value="2"
                status={ checked2 === '2' ? 'checked' : 'unchecked' }
                onPress={() => setChecked2('2')}
              />
              <Text style={styles.radio_txt}>1-2 วัน</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <RadioButton
                value="3"
                status={ checked2 === '3' ? 'checked' : 'unchecked' }
                onPress={() => setChecked2('3')}
              />
              <Text style={styles.radio_txt}>3-4 วัน</Text>
              <RadioButton
                value="4"
                status={ checked2 === '4' ? 'checked' : 'unchecked' }
                onPress={() => setChecked2('4')}
              />
              <Text style={styles.radio_txt}>มากกว่า 5 วัน</Text>
            </View>
          </View>
          <View style={{paddingVertical: 10}}>
            <Text style={styles.label_txt}>3. ท่านทำงานใช้แรงเยอะหรือไม่</Text>
            <View style={{flexDirection: 'row'}}>
              <RadioButton
                value="1"
                status={ checked3 === '1' ? 'checked' : 'unchecked' }
                onPress={() => setChecked3('1')}
              />
              <Text style={styles.radio_txt}>ใช่</Text>
              <RadioButton
                value="2"
                status={ checked3 === '2' ? 'checked' : 'unchecked' }
                onPress={() => setChecked3('2')}
              />
              <Text style={styles.radio_txt}>ไม่ใช่</Text>
            </View>
          </View>
          <View style={{paddingVertical: 10}}>
            <Text style={styles.label_txt}>4. ท่านพักผ่อนวันลัประมาณกี่ชั่วโมงต่อวัน</Text>
            <View style={{flexDirection: 'row'}}>
              <RadioButton
                value="1"
                status={ checked4 === '1' ? 'checked' : 'unchecked' }
                onPress={() => setChecked4('1')}
              />
              <Text style={styles.radio_txt}>{`< 6 ชั่วโมง`}</Text>
              <RadioButton
                value="2"
                status={ checked4 === '2' ? 'checked' : 'unchecked' }
                onPress={() => setChecked4('2')}
              />
              <Text style={styles.radio_txt}>{`6-8 ชั่วโมง`}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <RadioButton
                value="3"
                status={ checked4 === '3' ? 'checked' : 'unchecked' }
                onPress={() => setChecked4('3')}
              />
              <Text style={styles.radio_txt}>{`> 8 ชั่วโมง`}</Text>
            </View>
          </View>
          <View style={{paddingVertical: 10}}>
            <Text style={styles.label_txt}>5. การปัสวะใน 1 วัน</Text>
            <View style={{flexDirection: 'row'}}>
              <RadioButton
                value="1"
                status={ checked5 === '1' ? 'checked' : 'unchecked' }
                onPress={() => setChecked5('1')}
              />
              <Text style={styles.radio_txt}>1-2 ครั้ง</Text>
              <RadioButton
                value="2"
                status={ checked5 === '2' ? 'checked' : 'unchecked' }
                onPress={() => setChecked5('2')}
              />
              <Text style={styles.radio_txt}>3-4 ครั้ง</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <RadioButton
                value="3"
                status={ checked5 === '3' ? 'checked' : 'unchecked' }
                onPress={() => setChecked5('3')}
              />
              <Text style={styles.radio_txt}>{`> 4 ครั้ง`}</Text>
            </View>
          </View>
          <Button
            title="ประมวลผล"
            onPress={() => navigation.goBack()}
          />
        </View>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CE9C6F'
  },
  space: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CE9C6F',
    margin: 10,
  },
  content: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingHorizontal: 30
  },
  title_header: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#FFF',
  },
  logo: {
    width: 75,
    height: 75,
  },
  label_txt: {
    fontSize: 18
  },
  radio_txt: {
    fontSize: 18,
    alignSelf: 'center',
    paddingRight: 10
  },
});
