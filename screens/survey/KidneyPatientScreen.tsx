import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, TextInput, Platform, Button, ScrollView } from 'react-native';

import { Text, View } from '../../components/Themed';
import { Ionicons } from '@expo/vector-icons';
import { DataTable } from 'react-native-paper';
import { Card, Title } from 'react-native-paper';
import { ContentStyles } from '../../constants/CustomStyles';
import { StatusBar } from 'react-native';
import { TouchableOpacity } from 'react-native';
import UserInfo from '../../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import { FlatList } from 'react-native';
import axios from 'axios';
import { ROOT_URL } from '../../constants/Variable';
import { Alert } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
export default function KidneyPatientScreen({ navigation }: { navigation: any }) {
  var picsumImages1 = [
    { key: 1, name: '> 90', sodium: 'ระดับที่ 1', pic: require('./../../assets/images/kidney-16.jpg') }
  ];
  var picsumImages2 = [
    { key: 2, name: '60 - 90', sodium: 'ระดับที่ 2', pic: require('./../../assets/images/kidney-17.jpg') }
  ];
  var picsumImages3 = [
    { key: 3, name: '45 - 60', sodium: 'ระดับที่ 3a', pic: require('./../../assets/images/kidney-18.jpg') }
  ];
  var picsumImages4 = [
    { key: 4, name: '30 - 45', sodium: 'ระดับที่ 3b', pic: require('./../../assets/images/kidney-19.jpg') }
  ];
  var picsumImages5 = [
    { key: 5, name: '15 - 30', sodium: 'ระดับที่ 4', pic: require('./../../assets/images/kidney-20.jpg') },
  ];
  var picsumImages6 = [
    { key: 5, name: '<15', sodium: 'ระดับที่ 5', pic: require('./../../assets/images/kidney-21.jpg') },
  ];
  const [images, setImages] = React.useState([{}]);
  const [spinner, setSpinner] = useState(false);
  const [userInfo, setUserInfo] = React.useState(new UserInfo());
  const [kidneyInfo, setKidneyInfo] = React.useState({
    S: '',
    K: 0,
    A: 0,
    W: 0,
    R: 0,
  });
  const numColumns = 1;
  let isProcess: boolean = false;
  useEffect(() => {
    getData().then(
      (data) => {
        setUserInfo(data);
        console.log(data);
        if (data.weight != null) {
          kidneyInfo.W = data.weight
        }
        if (data.birthdate != null) {
          var now = moment(new Date()); //todays date
          var end = moment(data.birthdate); // another date
          var age = now.diff(end, 'years')
          kidneyInfo.A = age
        }
        if (data.sex != null) {
          if (data.sex == "F") {
            kidneyInfo.K = 1.04;
          } else {
            kidneyInfo.K = 1.23
          }
        }
        if (data.weight != null) {
          kidneyInfo.W = data.weight
        }
        console.log(kidneyInfo);


      },
      (err) => {
        console.log(err);
      }
    );
  }, []);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log(e);
    }
  }
  const failAlert = () => {
    setSpinner(false);
    Alert.alert(
      "แจ้งเตือน",
      "กรุณากรอกผลเลือด",
      [
        {
          text: "ตกลง",
          onPress: () => { },
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  }

  const failUserInfoAlert = () => {
    setSpinner(false);
    Alert.alert(
      "แจ้งเตือน",
      "กรุณาบันทึกข้อมูลส่วนตัวก่อนใช้งาน (อายุ,เพศ,น้ำหนัก)",
      [
        {
          text: "ตกลง",
          onPress: () => { },
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  }

  const onProcess = async () => {
    isProcess = true
    console.log(isProcess);
    setSpinner(true);
    kidneyInfo.R = (kidneyInfo.K * (140 - kidneyInfo.A) * kidneyInfo.W) / Number(kidneyInfo.S);
    if (Number(kidneyInfo.S) == 0) {
      failAlert();
    }
    else if (kidneyInfo.K == 0 || kidneyInfo.A == 0 || kidneyInfo.W == 0) {
      failUserInfoAlert();
    } else {
      if (kidneyInfo.R > 90 && kidneyInfo.R != Infinity) {
        setUserInfo({ ...userInfo, kidney_stage: "ระยะที่ 1 ไตปกติหรือเริ่มเสื่อม" })
        setImages(picsumImages1)
        onUpdate()
      } else if (kidneyInfo.R <= 89.99 && kidneyInfo.R >= 60) {
        setUserInfo({ ...userInfo, kidney_stage: "ระยะที่ 2 ไตเสื่อมเล็กน้อย" })
        setImages(picsumImages2)
        onUpdate()
      } else if (kidneyInfo.R <= 59.99 && kidneyInfo.R >= 45) {
        setUserInfo({ ...userInfo, kidney_stage: "ระยะที่ 3a ไตเสื่อมปลานกลางระดับ 1" })
        setImages(picsumImages3)
        onUpdate()
      } else if (kidneyInfo.R <= 44.99 && kidneyInfo.R >= 30) {
        setUserInfo({ ...userInfo, kidney_stage: "ระยะที่ 3b ไตเสื่อมปลานกลางระดับ 2" })
        setImages(picsumImages4)
        onUpdate()
      } else if (kidneyInfo.R <= 19.99 && kidneyInfo.R >= 15) {
        setUserInfo({ ...userInfo, kidney_stage: "ระยะที่ 4 ไตเสื่อมมาก" })
        setImages(picsumImages5)
        onUpdate()
      } else if (kidneyInfo.R <= 14.99 && kidneyInfo.R >= 1) {
        setUserInfo({ ...userInfo, kidney_stage: "ระยะที่ 5 ไตวายระยะสุดท้าย" })
        setImages(picsumImages6)
        onUpdate()
      } else {
        setImages(new Array())
      }
      console.log(kidneyInfo.R);
      console.log(images);
    }

  };


  const onUpdate = async () => {
    setSpinner(true);
    await axios.put(`${ROOT_URL}/users/`, userInfo)
      .then(response => {
        if (response.status == 200) {
          setSpinner(false);
        }
      }, (error) => {
        console.log(error);
        setSpinner(false);
      });
  };

  function renderItem({ item }: { item: any }) {
    return (
      <Image
        style={{ height: 155, width: "auto", padding: 0 }}
        source={item.pic}
      />
    );
  }

  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
      <Spinner
          visible={spinner}
          textContent={'กรุณารอสักครู่ ...'}
          textStyle={{ color: '#FFF' }}
        />
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 60, height: 60, alignSelf: 'center' }}
              source={require('./../../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75 }}
            source={require('./../../assets/images/menu/menu-servey.png')}
          />
          <Text style={ContentStyles.titleText}>ประเมินสุขภาพไต</Text>
        </View>
        <View style={styles.content}>
          <ScrollView>
            <View style={styles.center_view}>
              <Text style={styles.label_txt}>ผลเลือดการทำงานของไต (sCr)</Text>
            </View>
            <View style={styles.center_view}>
              <Text style={styles.label_txt}>Serum creatinine (µmol/L)</Text>
              <Text style={{ fontSize: 25, justifyContent: 'center', alignSelf: 'center', color: "#f5b43b", fontWeight: "bold" }}></Text>
            </View>
            <View style={styles.appButton}>
              <View style={styles.action}>
                <TextInput
                  placeholder="กรุณากรอกผลเลือด (sCr) "
                  placeholderTextColor="#666666"
                  style={[styles.textInput]}
                  autoCapitalize="none"
                  keyboardType={'numeric'}
                  onChangeText={(text) => {
                    if (text === '') {
                      setKidneyInfo({ ...kidneyInfo, S: '' })
                    } else if (/^\d+\.?\d*$/.test(text)) {
                      setKidneyInfo({ ...kidneyInfo, S: text })
                      setUserInfo({ ...userInfo, scr: text })
                    }
                  }}
                />
              </View>
            </View>
            <View style={styles.content}>
              <Button
                title="ประมวลผล"
                onPress={() => onProcess()}
              />
            </View>
            <View style={styles.content}>
              <Text style={{ fontSize: 25, justifyContent: 'center', alignSelf: 'center', color: "#f5b43b", fontWeight: "bold" }}>ผลการประมวลผล</Text>
              <Text style={{ fontSize: 25, justifyContent: 'center', alignSelf: 'center', color: "#f5b43b", fontWeight: "bold" }}></Text>
              <FlatList data={images} renderItem={renderItem} numColumns={numColumns} keyExtractor={(item, index) => index.toString()} />
              <Text style={{ fontSize: 25, justifyContent: 'center', alignSelf: 'center', color: "#f5b43b", fontWeight: "bold" }}></Text>
              <Text style={{ fontSize: 25, justifyContent: 'center', alignSelf: 'center', color: "#f5b43b", fontWeight: "bold" }}></Text>
            </View>

          </ScrollView>
        </View>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  imgHeaderViewSx: {
    flexBasis: '25%',
    backgroundColor: '#81E8B2',
    paddingRight: 20
  },
  emptyView: {
    paddingHorizontal: 10,
    flexBasis: '25%',
    backgroundColor: '#81E8B2'
  },
  content: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingHorizontal: 10
  },
  center_view: {
    justifyContent: 'center',
    alignSelf: 'center'
  },
  label_txt: {
    fontSize: 22,
    color: "#e82323",
    fontWeight: "bold"
  },
  action: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 30,
    padding: 10,
    borderBottomColor: '#f2f2f2',
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  appButton: {
    elevation: 8,
    backgroundColor: "#ffbd59",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginBottom: 10
  }
});
