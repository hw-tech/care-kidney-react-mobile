import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList, StatusBar, TouchableOpacity, Alert } from 'react-native';
import { useIsFocused } from "@react-navigation/native";

import { Text, View } from '../components/Themed';
import { ContentStyles, UtilsStyles } from '../constants/CustomStyles';

import { Ionicons } from '@expo/vector-icons';

import { Card, Title } from 'react-native-paper';
import Moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import UserInfo from '../data-model/UserInfo';
import axios from 'axios';
import { ROOT_URL } from '../constants/Variable';
import 'moment/min/locales'
import Spinner from 'react-native-loading-spinner-overlay';

export default function ConsumptionScreen({ navigation } : { navigation: any }) {
  const numColumns = 1;
  const isVisible = useIsFocused();

  const [consumption, setConsumption] = React.useState(Array());
  const [userInfo, setUserInfo] = React.useState(new UserInfo());
  const [spinner, setSpinner] = useState(false);

  const [sumConsumption, setSumConsumption] = React.useState(0);

  useEffect(() => {
    getData().then(
      (data) => {
        if (isVisible) {
          Moment.locale('th');
          setUserInfo(data);
          findConsumptionByUserId(data.user_login_id);
        }
      },
      (err) => {
        console.log(err);
      }
    );

    return () => {};
  }, [isVisible]);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log(e);
    }
  }

  const findConsumptionByUserId = async (id: number) => {
    setSpinner(true);
    await axios.get(`${ROOT_URL}/eat/today/${id}/${Moment(new Date()).format('YYYY-MM-DD')}`)
    .then(response => {
      if (response.status == 200) {
        setConsumption(response.data);

        if(response.data.length > 0){
          let sumSodium = 0;
          for(let item of response.data){
            sumSodium += (item.eat_amount * item.sodium)
          }

          setSumConsumption(sumSodium);
        } else {
          setSumConsumption(0);
        }

        setSpinner(false);
      }
    }, (error) => {
      console.log(error);
      setSpinner(false);
    });
  }

  function renderItem({ item } : { item: any }) {
    return (
      <Card style={{ flex: 1, margin: 10}} onPress={() => {}}>
        <Card.Content>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Card.Cover source={item.image_path == "" || item.image_path == null ? require('./../assets/images/food.png') : { uri: item.image_path }} style={{ height: 50, width: 50, backgroundColor: '#FFF' }} />
            <View>
              <Text style={{fontSize: 16}}>{item.food_name}</Text>
              <Text style={{fontSize: 14}}>({item.sodium} g)</Text>
            </View>
            <View>
              <Text style={{}}>เวลา {Moment(item.eat_date).subtract(7, 'h').format('HH:mm')}</Text>
              <Text style={{}}>จำนวน {item.eat_amount}</Text>
              <TouchableOpacity onPress={() => openDeleteAlert(item)}>
                <Text style={{fontSize: 18, backgroundColor: '#f44336', paddingHorizontal: 20, paddingVertical: 5, alignSelf: 'center', borderRadius: 10, color: '#FFF', marginTop: 10}}>ลบ</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Card.Content>
      </Card>
    );
  }

  const openDeleteAlert = (eatObj: any) =>{
    Alert.alert(
      "แจ้งเตือน",
      "คณต้องการลบรายการนี้หรือไม่ ?",
      [
        {
          text: "ยกเลิก",
          onPress: () => {},
          style: "cancel"
        },
        { 
          text: "ตกลง", 
          onPress: () => {
            deleteConsumption(eatObj);
          }
        }
      ],
      { cancelable: false }
    );
  }

  const deleteConsumption = async (eatObj: any) => {
    await axios.delete(`${ROOT_URL}/eat/${eatObj.user_eat_id}`)
    .then(response => {
      if (response.status == 200) {
        findConsumptionByUserId(userInfo.user_login_id);
      }
    }, (error) => {
      console.log(error);
    });
  }

  const openGraphModal = async () => {
    let currentMonth = Moment(new Date()).format('MM');
    let currentYear = Moment(new Date()).format('YYYY');
    let lastDay = Moment(new Date()).endOf('month').format('DD');

    setSpinner(true);

    let firstDayQuery = currentYear+"-"+currentMonth+"-01";
    let lastDayQuery;
    
    if(currentMonth == "12"){
      lastDayQuery = (Number(currentYear)+1)+"-01-01";
    } else {
      lastDayQuery = currentYear+"-"+(Number(currentMonth)+1)+"-01";
    }

    await axios.get(`${ROOT_URL}/eat/graph/${userInfo.user_login_id}/${firstDayQuery}/${lastDayQuery}`)
    .then(response => {
      if (response.status == 200) {
        setSpinner(false);
        navigation.push('History.Graph', {item: response.data, month: convertMonthName(currentMonth)});
      }
    }, (error) => {
      console.log(error);
      setSpinner(false);
    });
  }

  const convertMonthName = (month: string) => {
    let monthName: string = "";

    if(month == '01'){
      monthName = "มกราคม";
    } else if(month == '02'){
      monthName = "กุมภาพันธ์";
    } else if(month == '03'){
      monthName = "มีนาคม";
    } else if(month == '04'){
      monthName = "เมษายน";
    } else if(month == '05'){
      monthName = "พฤษภาคม";
    } else if(month == '06'){
      monthName = "มิถุนายน";
    } else if(month == '07'){
      monthName = "กรกฎาคม";
    } else if(month == '08'){
      monthName = "สิงหาคม";
    } else if(month == '09'){
      monthName = "กันยายน";
    } else if(month == '10'){
      monthName = "ตุลาคม";
    } else if(month == '11'){
      monthName = "พฤศจิกายน";
    } else {
      monthName = "ธันวาคม";
    }

    return monthName;
  }

  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()}/>
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 40, height: 40, alignSelf: 'center' }}
              source={require('./../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={styles.menuImg}>
          <Text style={ContentStyles.titleText}>ปริมาณโซเดียมสะสมวันนี้</Text>
          <Text style={styles.valueTextBorder}>{sumConsumption} mg</Text>
          <Text style={styles.subText}>ไม่ควรเกิน 2000 mg ต่อวัน</Text>
        </View>
        <View style={ContentStyles.whiteBackgroundColor}>

          <Spinner
            visible={spinner}
            textContent={'กรุณารอสักครู่ ...'}
            textStyle={{color: '#FFF'}}
          />

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 18, paddingVertical: 20}}>วันที่ {Moment().add(543, 'y').format('DD/MM/YYYY')}</Text>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity onPress={() => openGraphModal()}>
                <Text style={{fontSize: 18, backgroundColor: '#f0ad4e', paddingHorizontal: 10, paddingVertical: 5, alignSelf: 'center', borderRadius: 10, color: '#FFF', marginVertical: 10, marginLeft: 5}}>กราฟ</Text>
              </TouchableOpacity>
            </View>
          </View>
          <FlatList data={consumption} renderItem={renderItem} numColumns={numColumns} keyExtractor={(item, index) => index.toString()} />
          <TouchableOpacity style={styles.floatingBtn} onPress={() => {
            navigation.push('Consumption.FoodMenu', {item: null});
          }}>
              <Text style={{ fontSize: 30, color: '#fff' }}>+</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  emptyView: {
    paddingHorizontal: 10, 
    flexBasis: '25%', 
    backgroundColor: '#81E8B2'
  },
  subText: {
    fontSize: 15, 
    color: '#FFF',
    alignItems: 'center'
  },
  valueTextBorder: {
    fontSize: 20, 
    fontWeight: 'bold', 
    backgroundColor: '#fdfd96',
    color: '#81E8B2',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 20
  },
  menuImg: {
    backgroundColor: '#81E8B2', 
    borderBottomLeftRadius: 150, 
    borderBottomRightRadius: 150, 
    justifyContent: 'flex-start', 
    alignItems: 'center', 
    paddingBottom: 30, 
  },
  floatingBtn: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: "#81E8B2",
    width: 50,
    height: 50,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 10000
  },
});
