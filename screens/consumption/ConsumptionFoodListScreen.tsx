import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList, TouchableOpacity, Modal, TextInput, Button } from 'react-native';

import { Text, View } from '../../components/Themed';
import { Ionicons } from '@expo/vector-icons';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { ROOT_URL } from '../../constants/Variable';
import UserInfo from '../../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { ContentStyles, UtilsStyles } from '../../constants/CustomStyles';
import { StatusBar } from 'react-native';

import FoodDetail from '../../data-model/FoodDetail';
import UserEat from '../../data-model/UserEat';
import Spinner from 'react-native-loading-spinner-overlay';
import Moment from 'moment';

export default function ConsumptionFoodListScreen({ route, navigation }: { route: any, navigation: any }) {

  const numColumns = 1;
  const [foodDetailList, setFoodDetailList] = React.useState(Array());
  const [userInfo, setUserInfo] = React.useState(new UserInfo());
  const [foodDetail, setFoodDetail] = React.useState(new FoodDetail());

  const [consumption, setConsumption] = React.useState(new UserEat());

  const [failFlag, setFailFlag] = React.useState(false);
  const [spinner, setSpinner] = useState(false);

  const [modalVisible, setModalVisible] = useState({
    show: false,
    flag: ""
  });

  useEffect(() => {
    let isMounted = true;

    getData().then(
      (data) => {
        if (isMounted){
          setUserInfo(data);
          findFoodByFoodTypeId();
        }
      },
      (err) => {
        console.log(err);
      }
    );

    return () => { isMounted = false };
  }, []);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log(e);
    }
  }

  const findFoodByFoodTypeId = async () => {
    setSpinner(true);
    await axios.get(`${ROOT_URL}/foods/foodType/${route.params.itemId}/`)
      .then(response => {
        if (response.status == 200) {
          for (let _i = 0; _i < response.data.length; _i++) {
            response.data[_i].key = _i;
          }
          setFoodDetailList(response.data);
          setSpinner(false);
        }
      }, (error) => {
        setSpinner(false);
        Toast.show(error, Toast.SHORT)
      });
  }

  const addNewConsumption = async () => {
    if(consumption.eat_amount == "" || consumption.eat_amount == null){
      setFailFlag(true);
      return;
    }

    setSpinner(true);

    consumption.user_id = userInfo.user_login_id;
    consumption.create_user = userInfo.username;
    consumption.process = "ConsumptionScreen"

    consumption.food_id = foodDetail.food_id;
    consumption.food_name = foodDetail.food_name_th;
    consumption.sodium = foodDetail.sodium;
    consumption.eat_date = Moment(new Date(), 'DD/MM/yyyy').toDate();

    if(route.params.item == null){
      await axios.post(`${ROOT_URL}/eat/`, consumption)
      .then(response => {
        if (response.status == 200) {
          setConsumption(new UserEat());
  
          setSpinner(false);
          navigation.goBack();
          navigation.goBack();
        }
      }, (error) => {
        Toast.show(error, Toast.SHORT)
      });
    } else {
      consumption.eat_date = route.params.item;

      await axios.post(`${ROOT_URL}/eat/old`, consumption)
      .then(response => {
        if (response.status == 200) {
          setConsumption(new UserEat());
  
          setSpinner(false);
          navigation.goBack();
          navigation.goBack();
        }
      }, (error) => {
        setSpinner(false);
        Toast.show(error, Toast.SHORT)
      });
    }

    // set modal
    setModalVisible({...modalVisible, show: false});
  }

  function renderItem({ item }: { item: any }) {
    return (
      <TouchableOpacity onPress={() => {
        setFoodDetail(item);
        setModalVisible({...modalVisible, show: true}) 
      }} >
        <View style={{ height: 50, flexDirection: 'row', marginBottom: 10, borderBottomWidth: 0.5 }}>
          <Image
            style={{ height: 45, width: 50 ,borderRadius:5 }}
            source={item.image_path == "" || item.image_path == null ? require('./../../assets/images/food.png') :  {uri: item.image_path}}
          />
          <Text style={{ paddingHorizontal: 20, alignSelf: 'center', fontSize: 18 }}>{`${item.food_name_th} (โซเดียม ${item.sodium})`}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 60, height: 60, alignSelf: 'center' }}
              source={require('./../../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75 }}
            source={require('./../../assets/images/menu/menu-food_type.png')}
          />
          <Text style={ContentStyles.titleText}>{route.params.itemName}</Text>
        </View>
        <View style={styles.content}>

          <Spinner
            visible={spinner}
            textContent={'กรุณารอสักครู่ ...'}
            textStyle={{color: '#FFF'}}
          />

        {/* <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible.show}>
            <View style={{justifyContent: "center", alignItems: "center"}}>
              <View style={{padding: 20}}>
                <Image
                  style={{ width: 100, height: 100, alignSelf: 'center' }}
                  source={foodDetail.image_path == "" || foodDetail.image_path == null ? require('./../../assets/images/food.png') :  {uri: foodDetail.image_path}}
                />
                <Text style={{fontWeight: 'bold', fontSize: 25, paddingVertical: 10}}>{foodDetail.food_name_th}</Text>
                <Text style={{fontSize: 15, paddingBottom: 20}}>โซเดียม {foodDetail.sodium} mg ต่อ {foodDetail.unit}</Text>
                <Text>จำนวน</Text>
                <View style={UtilsStyles.action}>
                  <TextInput 
                    keyboardType="numeric"
                    placeholderTextColor="#666666"
                    style={[UtilsStyles.textInput]}
                    autoCapitalize="none"
                    value={consumption.eat_amount}
                    onChangeText={(text)=>{
                      if(text === ''){
                        setConsumption({...consumption, eat_amount: ''})
                      } else if (/^\d+\.?\d*$/.test(text)) {
                        setConsumption({...consumption, eat_amount: text})
                      }
                    }}
                  />
                </View>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                <View style={{flexBasis: '45%'}}>
                  <Button 
                    title="บันทึก" 
                    color="#81E8B2" 
                    onPress={() => {
                      addNewConsumption();
                      setModalVisible({...modalVisible, show: false});
                    }}>
                  </Button>
                </View>
                <View style={{flexBasis: '45%'}}>
                  <Button 
                    title="ยกเลิก" 
                    color="#81E8B2" 
                    onPress={() => {
                      setModalVisible({...modalVisible, show: false});
                    }}>
                  </Button>
                </View>
              </View>
            </View>
          </Modal> */}

          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible.show}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View style={{padding: 20}}>
                  <Image
                    style={{ width: 100, height: 100, alignSelf: 'center' }}
                    source={foodDetail.image_path == "" || foodDetail.image_path == null ? require('./../../assets/images/food.png') :  {uri: foodDetail.image_path}}
                  />
                  <Text style={{fontWeight: 'bold', fontSize: 25, paddingVertical: 10, textAlign: 'center'}}>{foodDetail.food_name_th}</Text>
                  <Text style={{fontSize: 15, paddingBottom: 20, textAlign: 'center'}}>โซเดียม {foodDetail.sodium} mg ต่อ {foodDetail.unit}</Text>
                  <Text>จำนวน</Text>
                  <View style={UtilsStyles.action}>
                    <TextInput 
                      keyboardType="numeric"
                      placeholderTextColor="#666666"
                      style={[UtilsStyles.textInput]}
                      autoCapitalize="none"
                      value={consumption.eat_amount}
                      onChangeText={(text)=>{
                        if(text === ''){
                          setConsumption({...consumption, eat_amount: ''})
                        } else if (/^\d+\.?\d*$/.test(text)) {
                          setConsumption({...consumption, eat_amount: text})
                        }
                      }}
                    />
                  </View>
                </View>
                {
                  failFlag == true && (
                    <View style={{paddingBottom: 10}}>
                      <Text style={{color: 'red'}}>* กรุณากรอกจำนวนที่รับประทาน</Text>
                    </View>
                  )
                }
                <View style={{flexDirection: 'row'}}>
                  <View style={{flexBasis: '45%'}}>
                    <Button 
                      title="บันทึก" 
                      color="#81E8B2" 
                      onPress={() => {
                        addNewConsumption();
                      }}>
                    </Button>
                  </View>
                  <View style={{flexBasis: '10%'}}></View>
                  <View style={{flexBasis: '45%'}}>
                    <Button 
                      title="ยกเลิก" 
                      color="#81E8B2" 
                      onPress={() => {
                        setModalVisible({...modalVisible, show: false});
                      }}>
                    </Button>
                  </View>
                </View>
              </View>
            </View>
          </Modal>

          <FlatList data={foodDetailList} renderItem={renderItem} numColumns={numColumns} keyExtractor={(item, index) => index.toString()}/>
        </View>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  imgHeaderViewSx: {
    flexBasis: '25%',
    backgroundColor: '#81E8B2',
    paddingRight: 20
  },
  emptyView: {
    paddingHorizontal: 10,
    flexBasis: '25%',
    backgroundColor: '#81E8B2'
  },
  content: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingHorizontal: 30
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
    backgroundColor: 'transparent'
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});