import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, ScrollView, TextInput, Button, StatusBar, Picker, Alert, YellowBox, Modal, TouchableOpacity } from 'react-native';

import { Text, View } from '../components/Themed';
import { ContentStyles, UtilsStyles } from '../constants/CustomStyles';

import { Ionicons } from '@expo/vector-icons';

import UserInfo from '../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import Spinner from 'react-native-loading-spinner-overlay';

import axios from 'axios';
import { ROOT_URL } from '../constants/Variable';

// 1. ให้ import firebase config
import { firebaseConfig } from '../constants/firebase';

// 2. ให้ import กล้อง
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

// 3. import firebase
import * as firebase from 'firebase/app';
import 'firebase/storage';

export default function ProfileAdminScreen({ route, navigation } : { route: any, navigation: any }) {
  const [userInfo, setUserInfo] = React.useState(new UserInfo());
  const [screenMode, setScreenMode] = React.useState("E");
  const [spinner, setSpinner] = useState(false);

  const [titleName, setTitleName] = React.useState([
    {key: 1, label: 'นาย', value: 'นาย'},
    {key: 2, label: 'นาง', value: 'นาง'},
    {key: 3, label: 'นางสาว', value: 'นางสาว'},
  ]);

  const [imgUrl, setImgUrl] = React.useState();

  const [modalVisible, setModalVisible] = useState({
    show: false,
    flag: ""
  });

  const [newPassword, setNewPassword] = useState({
    newPassword: "",
    reNewPassword: ""
  });

  useEffect(() => {
    let isMounted = true;

    if(route.params.item == "ADD_ADMIN_USER"){
      setScreenMode("A");
    } else if (route.params.item == "EDIT_ADMIN_USER"){
      setScreenMode("V");
      getDataSelected().then(
        (data) => {
          if (isMounted){
            setUserInfo(data);
            setImgUrl(data.image_path);
          }
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      setScreenMode("E");
      getData().then(
        (data) => {
          if (isMounted){
            setUserInfo(data);
            setImgUrl(data.image_path);
          }
        },
        (err) => {
          console.log(err);
        }
      );
    }

    // 5. init firebase
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
    // 6. ignore warning
    YellowBox.ignoreWarnings(['Setting a timer']);

    // 7. ขอ permission
    getPermissionAsync();

    return () => { isMounted = false };
  }, []);

  // 8. func ขอ permission
  const getPermissionAsync = async () => {
    if (Constants.platform?.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo');
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      console.log(e);
    }
  }

  const getDataSelected = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userSelected');
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      console.log(e);
    }
  }

  const updateUserInfo = async () => {
    userInfo.process = "ProfileScreen";
    userInfo.update_user = userInfo.username;

    setSpinner(true);

    let res = await axios.put(`${ROOT_URL}/users/admin/`, userInfo);
    if(res.status == 200){
      await axios.get(`${ROOT_URL}/users/info/${userInfo.user_id}/${userInfo.admin_flag}`)
      .then(response => {
        if(response.status == 200){
          console.log("get data");

          // get new data
          let item: UserInfo;
          item = response.data.userInfo[0];
          item.disease = response.data.disease;
          item.medicine = response.data.medicine;
          const jsonValue = JSON.stringify(item);
          
          // remove nad add new key
          AsyncStorage.removeItem('userInfo').then((value) => {
            AsyncStorage.setItem('userInfo', jsonValue);
          });

          // display toast
          // Toast.show('บันทึกข้อมูลสำเร็จ', Toast.SHORT);
          updateUserCompletedAlert();
          setSpinner(false);
        }
      }, (error) => {
        console.log(error);
        setSpinner(false);
      });
    }
  }

  const addNewUserInfo = async () => {
    userInfo.process = "RegisterScreen";
    userInfo.create_user = userInfo.username;
    userInfo.admin_flag = "Y";

    setSpinner(true);

    if(!userInfo.username || !userInfo.password  || !userInfo.firstname || !userInfo.lastname || !userInfo.mobile || !userInfo.address || !userInfo.email){
      Toast.show('กรุณากรอกข้อมูลให้ครบถ้วน', Toast.SHORT);
      return;
    }

    let res = await axios.get(`${ROOT_URL}/users/duplicate/admin/${userInfo.username}`);
    if(res.data.length == 0){
      axios.post(`${ROOT_URL}/users/admin`, userInfo)
      .then(response => {
        if(response.status == 200){
          setSpinner(false);
          navigation.goBack();
        }
      }, (error) => {
        console.log(error);
        setSpinner(false);
      });
    } else {
      setSpinner(false);
      Toast.show('ชื่อผู้ใช้งานซ้ำ', Toast.SHORT);
    }

  }

  const updateUserCompletedAlert = () =>{
    Alert.alert(
      "แจ้งเตือน",
      "บันทึกข้อมูลสำเร็จ",
      [
        {
          text: "ตกลง",
          onPress: () => {},
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  }

  // 9. function เปิดรูป พร้อม upload ขึ้น firebase
  const onChangeProfilePicture = async () =>{
    ImagePicker.launchImageLibraryAsync({ 
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1
    }).then((result)=>{ 
      if (!result.cancelled) {
        // User picked an image
        // console.log(result);
        
        const {height, width, type, uri} = result;
        return uriToBlob(uri);
      }
    }).then((blob)=>{
      if(typeof(blob) !== 'undefined'){
        uploadToFirebase(blob);
      }
      return;
    }).then((snapshot)=>{
      console.log("get file..");
    }).catch((error)=>{
      throw error;
    }); 
  };

  // 12. upload to firebase
  const uploadToFirebase = (blob: any) => {
    setSpinner(true);
    
    return new Promise((resolve, reject)=>{
      var storageRef = firebase.storage().ref();
      storageRef.child(`user/${userInfo.username}.jpg`).put(blob, {
        contentType: 'image/jpeg'
      }).then((snapshot)=>{
        blob.close();

        // download url
        storageRef.child(`user/${userInfo.username}.jpg`).getDownloadURL().then((result) => {
          userInfo.image_path = result;
          setImgUrl(result);
          
          setSpinner(false);
          resolve(snapshot);
        });

      }).catch((error)=>{
        reject(error);
      });
    });
  }  

  // 10. เปลี่ยน uri เป็น blob
  const uriToBlob = (uri: any) => {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function() {
        // return the blob
        resolve(xhr.response);
      };
      
      xhr.onerror = function() {
        // something went wrong
        reject(new Error('uriToBlob failed'));
      };

      // this helps us get a blob
      xhr.responseType = 'blob';

      xhr.open('GET', uri, true);
      xhr.send(null);
    });
  }

  const deleteUserByUserLoginId = async () => {
    setSpinner(true);
    await axios.delete(`${ROOT_URL}/users/${userInfo.user_login_id}/${userInfo.user_id}`)
    .then(response => {
      if(response.status == 200){
        setSpinner(false);
        navigation.goBack();
      }
    }, (error) => {
      console.log(error);
      setSpinner(false);
    });
  }

  const deleteUserConfirmAlert = () =>{
    Alert.alert(
      "แจ้งเตือน",
      "คุณต้องการลบผู้ใช้งานนี้หรือไม่ ?",
      [
        {
          text: "ยกเลิก",
          onPress: () => {},
          style: "cancel"
        },
        {
          text: "ตกลง",
          onPress: () => {
            deleteUserByUserLoginId();
          }
        }
      ],
      { cancelable: false }
    );
  }

  const onChangePassword = async () => {
    setSpinner(true);
    if(newPassword.newPassword == newPassword.reNewPassword){
      await axios.put(`${ROOT_URL}/users/change/${userInfo.username}/${newPassword.newPassword}`)
      .then(response => {
        if(response.status == 200){
          setSpinner(false);
          Toast.show('เปลี่ยนรหัสผ่าน สำเร็จ!', Toast.SHORT);
        }
      }, (error) => {
        console.log(error);
        setSpinner(false);
      });
    } else {
      setSpinner(false);
      Toast.show('รหัสผ่านใหม่กับยืนยันรหัสผ่านใหม่ ไม่ตรงกัน', Toast.SHORT);
    }
  }

  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()}/>
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 40, height: 40, alignSelf: 'center' }}
              source={require('./../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75, borderRadius: 75/2 }}
            source={imgUrl == "" || imgUrl == null ? require('./../assets/images/account.png') :  {uri: imgUrl}}
          />
          <Text style={ContentStyles.titleText}>ประวัติส่วนตัว</Text>
        </View>
        <View style={ContentStyles.whiteBackgroundColor}>

          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible.show}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View style={{padding: 20}}>
                  <View style={styles.customInputText}>
                    <TextInput 
                      secureTextEntry={true}
                      placeholderTextColor="#666666"
                      placeholder={"รหัสผ่านใหม่"}
                      style={[UtilsStyles.textInput]}
                      autoCapitalize="none"
                      onChangeText={(text)=>{
                        setNewPassword({...newPassword, newPassword: text})
                      }}
                    />
                  </View>
                  <View style={styles.customInputText}>
                    <TextInput 
                      secureTextEntry={true}
                      placeholderTextColor="#666666"
                      placeholder={"ยืนยันรหัสผ่านใหม่"}
                      style={[UtilsStyles.textInput]}
                      autoCapitalize="none"
                      onChangeText={(text)=>{
                        setNewPassword({...newPassword, reNewPassword: text})
                      }}
                    />
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flexBasis: '45%'}}>
                    <Button 
                      title="บันทึก" 
                      color="#81E8B2" 
                      onPress={() => {
                        onChangePassword();
                        setModalVisible({...modalVisible, show: false});
                      }}>
                    </Button>
                  </View>
                  <View style={{flexBasis: '10%'}}></View>
                  <View style={{flexBasis: '45%'}}>
                    <Button 
                      title="ยกเลิก" 
                      color="#81E8B2" 
                      onPress={() => {
                        setModalVisible({...modalVisible, show: false});
                      }}>
                    </Button>
                  </View>
                </View>
              </View>
            </View>
          </Modal>

          <Spinner
            visible={spinner}
            textContent={'กรุณารอสักครู่ ...'}
            textStyle={{color: '#FFF'}}
          />

          <ScrollView>
            {
              screenMode == "A" && (
                <View>
                  <Text>ชื่อผู้ใช้งาน</Text>
                  <View style={UtilsStyles.action}>
                    <TextInput 
                      placeholderTextColor="#666666"
                      style={[UtilsStyles.textInput]}
                      autoCapitalize="none"
                      onChangeText={(text)=>{
                        setUserInfo({...userInfo, username: text})
                      }}
                    />
                  </View>
                </View>
              )
            }
            {
              screenMode == "A" && (
                <View>
                  <Text>รหัสผ่าน</Text>
                  <View style={UtilsStyles.action}>
                    <TextInput 
                      secureTextEntry={true}
                      placeholderTextColor="#666666"
                      style={[UtilsStyles.textInput]}
                      autoCapitalize="none"
                      onChangeText={(text)=>{
                        setUserInfo({...userInfo, password: text})
                      }}
                    />
                  </View>
                </View>
              )
            }
            {
              screenMode != "A" && (
                <View>
                  <Text>ชื่อผู้ใช้งาน</Text>
                  <View style={UtilsStyles.action}>
                    <TextInput 
                      placeholderTextColor="#666666"
                      style={[UtilsStyles.textInput]}
                      autoCapitalize="none"
                      value={userInfo.username}
                      editable={false}
                    />
                  </View>
                  <Text>รหัสผ่าน</Text>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flexBasis: '70%'}}>
                      <TextInput 
                        placeholderTextColor="#666666"
                        autoCapitalize="none"
                        value={"**********"}
                        editable={false}
                      />
                    </View>
                    <View style={{flexBasis: '30%'}}>
                      <TouchableOpacity onPress={() => {setModalVisible({...modalVisible, show: true});}}>
                        <Text style={{fontSize: 16, backgroundColor: '#81E8B2', paddingHorizontal: 20, paddingVertical: 5, alignSelf: 'center', borderRadius: 10, color: '#FFF'}}>เปลี่ยน</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              )
            }
            <Text>คำนำหน้า</Text>
            <View style={UtilsStyles.actionPicker}>
              <Picker
                selectedValue={userInfo.title_name}
                style={{}}
                onValueChange={(itemValue, itemIndex) => {
                  setUserInfo({...userInfo, title_name: itemValue});
                }} >
                <Picker.Item label="กรุณาเลือก..." value="" />
                {
                  titleName.map((item, key) => {
                    return(
                      <Picker.Item key={key} label={item.label} value={item.value} />
                    )
                  })
                }
                {/* <Picker.Item label="นาง" value="นาง" />
                <Picker.Item label="นางสาว" value="นางสาว" /> */}
              </Picker>
            </View>
            <Text>ชื่อ</Text>
            <View style={UtilsStyles.action}>
              <TextInput 
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                value={userInfo.firstname}
                onChangeText={(text)=>{
                  setUserInfo({...userInfo, firstname: text})
                }}
              />
            </View>
            <Text>นามสกุล</Text>
            <View style={UtilsStyles.action}>
              <TextInput 
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                value={userInfo.lastname}
                onChangeText={(text)=>{
                  setUserInfo({...userInfo, lastname: text})
                }}
              />
            </View>
            <Text>เบอร์ติดต่อ</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                keyboardType={"number-pad"}
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                value={userInfo.mobile}
                onChangeText={(text)=>{
                  setUserInfo({...userInfo, mobile: text})
                }}
              />
            </View>
            <Text>ที่อยู่</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                value={userInfo.address}
                onChangeText={(text)=>{
                  setUserInfo({...userInfo, address: text})
                }}
              />
            </View>
            <Text>อีเมล์</Text>
            <View style={UtilsStyles.action}>
              <TextInput
                placeholderTextColor="#666666"
                style={[UtilsStyles.textInput]}
                autoCapitalize="none"
                value={userInfo.email}
                onChangeText={(text)=>{
                  setUserInfo({...userInfo, email: text})
                }}
              />
            </View>
            {
              screenMode == "E" && (
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                  <Text>รูปผู้ใช้งาน</Text>
                  <Button 
                    title="เปลี่ยน" 
                    color="#81E8B2" 
                    onPress={() => {
                      onChangeProfilePicture();
                    }}>
                  </Button>
                </View>
              )
            }
            {
              screenMode == "A" && (
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                  <Text>รูปผู้ใช้งาน</Text>
                  <Button 
                    title="เปลี่ยน" 
                    color="#81E8B2" 
                    onPress={() => {
                      onChangeProfilePicture();
                    }}>
                  </Button>
                </View>
              )
            }
            {
              screenMode != "V" && (
                <View style={{ marginTop: 20 }}>
                  <Button
                    title="บันทึก"
                    color="#81E8B2"
                    onPress={() => {
                      if(screenMode == "A"){
                        addNewUserInfo();
                      } else {
                        updateUserInfo();
                      }
                    }}
                  />
                </View>
              )
            }
            {
              screenMode == "V" && (
                <View style={{ marginTop: 20 }}>
                  <Button
                    title="บันทึกแก้ไข"
                    color="#81E8B2"
                    onPress={() => {
                      updateUserInfo();
                    }}
                  />
                </View>
              )
            }
            {
              screenMode == "V" && (
                <View style={{ marginTop: 20 }}>
                  <Button
                    title="ลบผู้ใช้งาน"
                    color="#f44336"
                    onPress={() => {
                      deleteUserConfirmAlert();
                    }}
                  />
                </View>
              )
            }
          </ScrollView>
        </View>
      </SafeAreaView>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  emptyView: {
    paddingHorizontal: 10, 
    flexBasis: '25%', 
    backgroundColor: '#81E8B2'
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
    backgroundColor: 'transparent'
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  customInputText: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
    width: "100%"
  }
});
