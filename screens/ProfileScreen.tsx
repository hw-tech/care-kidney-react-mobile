import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, ScrollView, TextInput, Button, StatusBar, Picker, TouchableOpacity, Modal, Alert, YellowBox } from 'react-native';

import { Text, View } from '../components/Themed';
import { ContentStyles, UtilsStyles } from '../constants/CustomStyles';
import { DataTable } from 'react-native-paper';
import SegmentedControlTab from 'react-native-segmented-control-tab';

import { Ionicons } from '@expo/vector-icons';
import DateTimePicker from "react-native-modal-datetime-picker";
import { TextInputMask } from 'react-native-masked-text';

import UserInfo from '../data-model/UserInfo';
import Disease from '../data-model/Disease';
import Medicine from '../data-model/Medicine';
import AsyncStorage from '@react-native-community/async-storage';
import Moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';

import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { ROOT_URL } from '../constants/Variable';

// 1. ให้ import firebase config
import { firebaseConfig } from '../constants/firebase';

// 2. ให้ import กล้อง
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

// 3. import firebase
import * as firebase from 'firebase/app';
import 'firebase/storage';

export default function ProfileScreen({ route, navigation } : { route: any, navigation: any }) {
  const [show, setShow] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [screenMode, setScreenMode] = React.useState("");
  const [spinner, setSpinner] = useState(false);
  const [dateText, setDateText] = React.useState("");

  const [modalDiseaseVisible, setModalDiseaseVisible] = useState({
    show: false,
    flag: ""
  });
  const [modalMedicineVisible, setModalMedicineVisible] = useState({
    show: false,
    flag: ""
  });

  const [userInfo, setUserInfo] = React.useState(new UserInfo());

  const [diseaseList, setDiseaseList] = React.useState(Array());
  const [disease, setDisease] = React.useState(new Disease());

  const [medicineList, setMedicineList] = React.useState(Array());
  const [medicine, setMedicine] = React.useState(new Medicine());

  const [titleName, setTitleName] = React.useState([
    {key: 1, label: 'นาย', value: 'นาย'},
    {key: 2, label: 'นาง', value: 'นาง'},
    {key: 3, label: 'นางสาว', value: 'นางสาว'},
  ]);

  const [newPassword, setNewPassword] = useState({
    newPassword: "",
    reNewPassword: ""
  });

  const [modalVisible, setModalVisible] = useState({
    show: false,
    flag: ""
  });

  // 4. สร้างตัวแปรเอาไว้เก็บรูป [ใช้เก็บ URL แทน]
  // var blobImg: any;
  const [imgUrl, setImgUrl] = React.useState();

  useEffect(() => {
    // 13. สร้างตัวแปร check mounted
    let isMounted = true;
    
    if(route.params.item === "EDIT_USER"){
      setScreenMode("V");
      getDataSelected().then(
        (data) => {
          if(typeof(data.disease) !== 'undefined' && data.disease.length){
            for (let _i = 0; _i < data.disease.length; _i++) {
              data.disease[_i].key = _i;
            }
          } else {
            data.disease = [];
          }
  
          if(typeof(data.medicine) !== 'undefined' && data.medicine.length){
            for (let _i = 0; _i < data.medicine.length; _i++) {
              data.medicine[_i].key = _i;
            }
          } else {
            data.medicine = [];
          }

          // 14. check isMounted
          if (isMounted){
            setUserInfo(data);
            setDiseaseList(data.disease);
            setMedicineList(data.medicine);
            setImgUrl(data.image_path);
            
            setDateText(Moment(data.birthdate).format('DD/MM/YYYY'));
          }
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      setScreenMode("E");
      getData().then(
        (data) => {
          if(typeof(data.disease) !== 'undefined' && data.disease.length){
            for (let _i = 0; _i < data.disease.length; _i++) {
              data.disease[_i].key = _i;
            }
          } else {
            data.disease = [];
          }
  
          if(typeof(data.medicine) !== 'undefined' && data.medicine.length){
            for (let _i = 0; _i < data.medicine.length; _i++) {
              data.medicine[_i].key = _i;
            }
          } else {
            data.medicine = [];
          }
  
          if (isMounted){
            setUserInfo(data);
            setDiseaseList(data.disease);
            setMedicineList(data.medicine);
            setImgUrl(data.image_path);

            setDateText(Moment(data.birthdate).format('DD/MM/YYYY'));
          }
        },
        (err) => {
          console.log(err);
        }
      );
    }

    // 5. init firebase
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
    // 6. ignore warning
    YellowBox.ignoreWarnings(['Setting a timer']);

    // 7. ขอ permission
    getPermissionAsync();

    // 15. set false
    return () => { isMounted = false };
  }, []);


  // 8. func ขอ permission
  const getPermissionAsync = async () => {
    if (Constants.platform?.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo');
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      console.log(e);
    }
  }

  const getDataSelected = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userSelected');
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      console.log(e);
    }
  }

  const calculateBMI = () => {
    let bmi, weight, height;
    if(userInfo.weight && userInfo.height){
      weight = Number(userInfo.weight);
      height = Number(userInfo.height);

      bmi = weight / Math.pow((height / 100), 2);
      setUserInfo({...userInfo, bmi: String(bmi.toFixed(2))});
    }
  }

  const updateUserInfo = async () => {
    // if(userInfo.identification_id.length != 13){
    //   Toast.show('เลขบัตรประชาชนไม่ครบหรือเกิน 13 หลัก', Toast.SHORT);
    //   return;
    // }

    setSpinner(true);

    userInfo.process = "ProfileScreen";
    userInfo.update_user = userInfo.username;
    userInfo.disease = diseaseList;
    userInfo.medicine = medicineList;

    // upload when save
    // 11. อัพโหลดรูปไปยัง firebase [ไม่ใช้แล้ว]
    // if(blobImg != null){
    //   await uploadToFirebase(blobImg);
    // }

    // Find duplicate
    let duplicate = await axios.get(`${ROOT_URL}/users/duplicate/${userInfo.username}/${userInfo.identification_id}`);

    if(duplicate.data.length == 0){
      let res = await axios.put(`${ROOT_URL}/users/`, userInfo);
      if(res.status == 200){
        await axios.get(`${ROOT_URL}/users/info/${userInfo.user_id}/${userInfo.admin_flag}`)
        .then(response => {
          if(response.status == 200){
            // get new data
            let item: UserInfo;
            item = response.data.userInfo[0];
            item.disease = response.data.disease;
            item.medicine = response.data.medicine;
            const jsonValue = JSON.stringify(item);
            
            // remove nad add new key
            if(screenMode == 'E'){
              AsyncStorage.removeItem('userInfo').then((value) => {
                AsyncStorage.setItem('userInfo', jsonValue);
              });
            }

            // display toast
            // Toast.show('บันทึกข้อมูลสำเร็จ', Toast.SHORT);
            updateUserCompletedAlert();
            setSpinner(false);
          }
        }, (error) => {
          console.log(error);
          setSpinner(false);
        });
      }
    } else {
      setSpinner(false);
      Toast.show('ชื่อผู้ใช้งานหรือเลขบัตรประชาชนซ้ำ', Toast.SHORT);
    }
    
  }

  const updateUserCompletedAlert = () =>{
    Alert.alert(
      "แจ้งเตือน",
      "บันทึกข้อมูลสำเร็จ",
      [
        {
          text: "ตกลง",
          onPress: () => {},
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  }

  // 9. function เปิดรูป พร้อม upload ขึ้น firebase
  const onChangeProfilePicture = async () =>{
    ImagePicker.launchImageLibraryAsync({ 
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1
    }).then((result)=>{ 
      if (!result.cancelled) {
        // User picked an image
        // console.log(result);
        
        const {height, width, type, uri} = result;
        return uriToBlob(uri);
      }
    }).then((blob)=>{
      if(typeof(blob) !== 'undefined'){
        uploadToFirebase(blob);
      }
      return;
    }).then((snapshot)=>{
      console.log("get file..");
    }).catch((error)=>{
      throw error;
    }); 
  };

  // 12. upload to firebase
  const uploadToFirebase = (blob: any) => {
    setSpinner(true);

    return new Promise((resolve, reject)=>{
      var storageRef = firebase.storage().ref();
      storageRef.child(`user/${userInfo.username}.jpg`).put(blob, {
        contentType: 'image/jpeg'
      }).then((snapshot)=>{
        blob.close();

        // download url
        storageRef.child(`user/${userInfo.username}.jpg`).getDownloadURL().then((result) => {
          userInfo.image_path = result;
          setImgUrl(result);
          
          setSpinner(false);
          resolve(snapshot);
        });

      }).catch((error)=>{
        reject(error);
      });
    });
  }  

  // 10. เปลี่ยน uri เป็น blob
  const uriToBlob = (uri: any) => {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function() {
        // return the blob
        resolve(xhr.response);
      };
      
      xhr.onerror = function() {
        // something went wrong
        reject(new Error('uriToBlob failed'));
      };

      // this helps us get a blob
      xhr.responseType = 'blob';

      xhr.open('GET', uri, true);
      xhr.send(null);
    });
  }

  const deleteUserByUserLoginId = async () => {
    setSpinner(true);
    await axios.delete(`${ROOT_URL}/users/${userInfo.user_login_id}/${userInfo.user_id}`)
    .then(response => {
      if(response.status == 200){
        setSpinner(false);
        navigation.goBack();
      }
    }, (error) => {
      console.log(error);
      setSpinner(false);
    });
  }

  const deleteUserConfirmAlert = () =>{
    Alert.alert(
      "แจ้งเตือน",
      "คุณต้องการลบผู้ใช้งานนี้หรือไม่ ?",
      [
        {
          text: "ยกเลิก",
          onPress: () => {},
          style: "cancel"
        },
        {
          text: "ตกลง",
          onPress: () => {
            deleteUserByUserLoginId();
          }
        }
      ],
      { cancelable: false }
    );
  }

  const onChangePassword = async () => {
    setSpinner(true);
    if(newPassword.newPassword == newPassword.reNewPassword){
      await axios.put(`${ROOT_URL}/users/change/${userInfo.username}/${newPassword.newPassword}`)
      .then(response => {
        if(response.status == 200){
          setSpinner(false);
          Toast.show('เปลี่ยนรหัสผ่าน สำเร็จ!', Toast.SHORT);
        }
      }, (error) => {
        console.log(error);
        setSpinner(false);
      });
    } else {
      setSpinner(false);
      Toast.show('รหัสผ่านใหม่กับยืนยันรหัสผ่านใหม่ ไม่ตรงกัน', Toast.SHORT);
    }
  }

  function renderAgeText() {
    if (userInfo.birthdate == null) {
      return (
        <View>
          <Text>อายุ</Text>
          <View style={{paddingTop: 5, paddingBottom: 10, paddingHorizontal: 10}}>
            <Text>กรุณากรอกวันเกิด</Text>
          </View>
        </View>
      )
    } else {
      if(Moment().diff(Moment(userInfo.birthdate).format('yyyy-MM-DD'), 'years') + 543 > 200){
        return (
          <View>
            <Text>อายุ</Text>
            <View style={{paddingTop: 5, paddingBottom: 10, paddingHorizontal: 10}}>
              <Text>{Moment().diff(Moment(userInfo.birthdate).format('yyyy-MM-DD'), 'years')} ปี</Text>
            </View>
          </View>
        )
      } else {
        return (
          <View>
            <Text>อายุ</Text>
            <View style={{paddingTop: 5, paddingBottom: 10, paddingHorizontal: 10}}>
              <Text>{Moment().diff(Moment(userInfo.birthdate).format('yyyy-MM-DD'), 'years') + 543} ปี</Text>
            </View>
          </View>
        )
      }
    }
  }

  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()}/>
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 40, height: 40, alignSelf: 'center' }}
              source={require('./../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75, borderRadius: 75/2 }}
            source={imgUrl == "" || imgUrl == null ? require('./../assets/images/account.png') :  {uri: imgUrl}}
          />
          <Text style={ContentStyles.titleText}>ประวัติส่วนตัว</Text>
        </View>
        <View style={ContentStyles.whiteBackgroundColor}>

          <Modal
            animationType="slide"
            transparent={false}
            visible={modalDiseaseVisible.show}>
            <View style={{padding: 20}}>
              <Text>ชื่อโรค</Text>
              <View style={UtilsStyles.action}>
                <TextInput 
                  placeholderTextColor="#666666"
                  style={[UtilsStyles.textInput]}
                  autoCapitalize="none"
                  value={disease.disease_name}
                  onChangeText={(text)=>{
                    setDisease({...disease, disease_name: text})
                  }}
                />
              </View>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <View style={{flexBasis: '45%'}}>
                <Button 
                  title="บันทึก" 
                  color="#81E8B2" 
                  onPress={() => {
                    if(modalDiseaseVisible.flag == "A"){
                      setDiseaseList([...diseaseList, {...disease, key: diseaseList.length}]);
                    } else {
                      let item = [...diseaseList];
                      item[disease.key] = disease;
                      setDiseaseList(item);
                    }

                    setDisease(new Disease());
                    setModalDiseaseVisible({...modalDiseaseVisible, show: false});
                  }}>
                </Button>
              </View>
              <View style={{flexBasis: '45%'}}>
                <Button 
                  title="ยกเลิก" 
                  color="#81E8B2" 
                  onPress={() => {
                    setDisease(new Disease());
                    setModalDiseaseVisible({...modalDiseaseVisible, show: false});
                  }}>
                </Button>
              </View>
            </View>
          </Modal>

          <Modal
            animationType="slide"
            transparent={false}
            visible={modalMedicineVisible.show}>
            <View style={{padding: 20}}>
              <Text>ชื่อยา (ภาษาไทย)</Text>
              <View style={UtilsStyles.action}>
                <TextInput 
                  placeholderTextColor="#666666"
                  style={[UtilsStyles.textInput]}
                  autoCapitalize="none"
                  value={medicine.medicine_name_th}
                  onChangeText={(text)=>{
                    setMedicine({...medicine, medicine_name_th: text})
                  }}
                />
              </View>
              <Text>ชื่อยา (ภาษาอังกฤษ)</Text>
              <View style={UtilsStyles.action}>
                <TextInput 
                  placeholderTextColor="#666666"
                  style={[UtilsStyles.textInput]}
                  autoCapitalize="none"
                  value={medicine.medicine_name_en}
                  onChangeText={(text)=>{
                    setMedicine({...medicine, medicine_name_en: text})
                  }}
                />
              </View>
              <Text>วิธีการใช้งาน</Text>
              <View style={UtilsStyles.action}>
                <TextInput 
                  placeholderTextColor="#666666"
                  style={[UtilsStyles.textInput]}
                  autoCapitalize="none"
                  value={medicine.medicine_taking}
                  onChangeText={(text)=>{
                    setMedicine({...medicine, medicine_taking: text})
                  }}
                />
              </View>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <View style={{flexBasis: '45%'}}>
                <Button 
                  title="บันทึก" 
                  color="#81E8B2" 
                  onPress={() => {
                    if(modalMedicineVisible.flag == "A"){
                      setMedicineList([...medicineList, {...medicine, key: medicineList.length}]);
                    } else {
                      let item = [...medicineList];
                      item[medicine.key] = medicine;
                      setMedicineList(item);
                    }

                    setMedicine(new Medicine());
                    setModalMedicineVisible({...modalMedicineVisible, show: false});
                  }}>
                </Button>
              </View>
              <View style={{flexBasis: '45%'}}>
                <Button 
                  title="ยกเลิก" 
                  color="#81E8B2" 
                  onPress={() => {
                    setMedicine(new Medicine());
                    setModalMedicineVisible({...modalMedicineVisible, show: false})
                  }}>
                </Button>
              </View>
            </View>
          </Modal>

          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible.show}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View style={{padding: 20}}>
                  <View style={styles.customInputText}>
                    <TextInput 
                      secureTextEntry={true}
                      placeholderTextColor="#666666"
                      placeholder={"รหัสผ่านใหม่"}
                      style={[UtilsStyles.textInput]}
                      autoCapitalize="none"
                      onChangeText={(text)=>{
                        setNewPassword({...newPassword, newPassword: text})
                      }}
                    />
                  </View>
                  <View style={styles.customInputText}>
                    <TextInput 
                      secureTextEntry={true}
                      placeholderTextColor="#666666"
                      placeholder={"ยืนยันรหัสผ่านใหม่"}
                      style={[UtilsStyles.textInput]}
                      autoCapitalize="none"
                      onChangeText={(text)=>{
                        setNewPassword({...newPassword, reNewPassword: text})
                      }}
                    />
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flexBasis: '45%'}}>
                    <Button 
                      title="บันทึก" 
                      color="#81E8B2" 
                      onPress={() => {
                        onChangePassword();
                        setModalVisible({...modalVisible, show: false});
                      }}>
                    </Button>
                  </View>
                  <View style={{flexBasis: '10%'}}></View>
                  <View style={{flexBasis: '45%'}}>
                    <Button 
                      title="ยกเลิก" 
                      color="#81E8B2" 
                      onPress={() => {
                        setModalVisible({...modalVisible, show: false});
                      }}>
                    </Button>
                  </View>
                </View>
              </View>
            </View>
          </Modal>

          <SegmentedControlTab
            values={['ข้อมูลส่วนตัว', 'โรคประจำตัว', 'ยาประจำตัว']}
            selectedIndex={selectedIndex}
            onTabPress={(index: number) => {
              setSelectedIndex(index);
            }}
            tabStyle={{marginBottom: 10, borderColor: '#81E8B2'}}
            activeTabStyle={{ backgroundColor: '#81E8B2'}}
            tabTextStyle={{ color: '#26ce76'}}
          />

          <Spinner
            visible={spinner}
            textContent={'กรุณารอสักครู่ ...'}
            textStyle={{color: '#FFF'}}
          />

          <ScrollView>
            {selectedIndex === 0 && (
              <View>
                <Text>ชื่อผู้ใช้งาน</Text>
                <View style={UtilsStyles.action}>
                  <TextInput 
                    placeholderTextColor="#666666"
                    style={[UtilsStyles.textInput]}
                    autoCapitalize="none"
                    value={userInfo.username}
                    editable={false}
                  />
                </View>
                <Text>รหัสผ่าน</Text>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flexBasis: '70%'}}>
                      <TextInput 
                        placeholderTextColor="#666666"
                        autoCapitalize="none"
                        value={"**********"}
                        editable={false}
                      />
                    </View>
                    <View style={{flexBasis: '30%'}}>
                      <TouchableOpacity onPress={() => {setModalVisible({...modalVisible, show: true});}}>
                        <Text style={{fontSize: 16, backgroundColor: '#81E8B2', paddingHorizontal: 20, paddingVertical: 5, alignSelf: 'center', borderRadius: 10, color: '#FFF'}}>เปลี่ยน</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                <Text>คำนำหน้า</Text>
                <View style={UtilsStyles.actionPicker}>
                  <Picker
                    selectedValue={userInfo.title_name}
                    style={{}}
                    onValueChange={(itemValue, itemIndex) => {
                      setUserInfo({...userInfo, title_name: itemValue});
                    }} >
                    <Picker.Item label="กรุณาเลือก..." value="" />
                    {
                      titleName.map((item, key) => {
                        return(
                          <Picker.Item key={key} label={item.label} value={item.value} />
                        )
                      })
                    }
                    {/* <Picker.Item label="นาง" value="นาง" />
                    <Picker.Item label="นางสาว" value="นางสาว" /> */}
                  </Picker>
                </View>
                <Text>ชื่อ</Text>
                <View style={UtilsStyles.action}>
                  <TextInput 
                    placeholderTextColor="#666666"
                    style={[UtilsStyles.textInput]}
                    autoCapitalize="none"
                    value={userInfo.firstname}
                    onChangeText={(text)=>{
                      setUserInfo({...userInfo, firstname: text})
                    }}
                  />
                </View>
                <Text>นามสกุล</Text>
                <View style={UtilsStyles.action}>
                  <TextInput 
                    placeholderTextColor="#666666"
                    style={[UtilsStyles.textInput]}
                    autoCapitalize="none"
                    value={userInfo.lastname}
                    onChangeText={(text)=>{
                      setUserInfo({...userInfo, lastname: text})
                    }}
                  />
                </View>
                {/* <Text>เลขบัตรประชาชน</Text>
                <View style={UtilsStyles.action}>
                  <TextInput 
                    maxLength={13}
                    keyboardType={"number-pad"}
                    placeholderTextColor="#666666"
                    style={[UtilsStyles.textInput]}
                    autoCapitalize="none"
                    value={userInfo.identification_id}
                    onChangeText={(text)=>{
                      setUserInfo({...userInfo, identification_id: text})
                    }}
                  />
                </View> */}
                {/* <TouchableOpacity onPress={() => setShow(true)}>
                  <Text>วันเกิด</Text>
                  <View style={UtilsStyles.action}>
                    <Text style={[UtilsStyles.placeholderCustom]}>{userInfo.birthdate != null ? Moment(userInfo.birthdate).format('DD/MM/yyyy') : 'กรุณาเลือก...'}</Text>
                    <DateTimePicker
                      isVisible={show}
                      onConfirm={(date: Date) => {
                        setUserInfo({...userInfo, birthdate: date});
                        setShow(false);
                      }}
                      onCancel={() => setShow(false)}
                      locale={'th_TH'}
                    />
                  </View>
                </TouchableOpacity> */}
                <Text>วันเกิด (วัน/เดือน/ปี พ.ศ.)</Text>
                <View style={UtilsStyles.action}>
                  <TextInputMask
                    style={{width: '100%'}}
                    type={'datetime'}
                    options={{
                      format: 'DD/MM/YYYY'
                    }}
                    value={dateText}
                    onChangeText={text => {
                      setDateText(text);

                      if(text.length == 10){
                        setUserInfo({...userInfo, birthdate: Moment(text, 'DD/MM/yyyy').toDate()});
                      }
                    }}
                  />
                </View>
                { renderAgeText() }
                <Text>เพศ</Text>
                <View style={UtilsStyles.actionPicker}>
                  <Picker
                    selectedValue={userInfo.sex}
                    style={{}}
                    onValueChange={(itemValue, itemIndex) => {
                      setUserInfo({...userInfo, sex: itemValue});
                    }} >
                    <Picker.Item label="กรุณาเลือก..." value="" />
                    <Picker.Item label="ชาย" value="M" />
                    <Picker.Item label="หญิง" value="F" />
                  </Picker>
                </View>
                <Text>น้ำหนัก</Text>
                <View style={UtilsStyles.action}>
                  <TextInput 
                    keyboardType="numeric"
                    placeholderTextColor="#666666"
                    placeholder="กิโลกรัม"
                    style={[UtilsStyles.textInput]}
                    autoCapitalize="none"
                    value={userInfo.weight != null ? String(userInfo.weight) : ''}
                    onChangeText={(text)=>{
                      if(text === ''){
                        setUserInfo({...userInfo, weight: ''})
                      } else if (/^\d+\.?\d*$/.test(text)) {
                        setUserInfo({...userInfo, weight: text})
                      }
                    }}
                    onEndEditing={()=>{
                      calculateBMI();
                    }}
                  />
                </View>
                <Text>ส่วนสูง</Text>
                <View style={UtilsStyles.action}>
                  <TextInput 
                    keyboardType="numeric"
                    placeholderTextColor="#666666"
                    placeholder="เซนติเมตร"
                    style={[UtilsStyles.textInput]}
                    autoCapitalize="none"
                    value={userInfo.height != null ? String(userInfo.height) : ''}
                    onChangeText={(text)=>{
                      if(text === ''){
                        setUserInfo({...userInfo, height: ''})
                      } else if (/^\d+\.?\d*$/.test(text)) {
                        setUserInfo({...userInfo, height: text})
                      }
                    }}
                    onEndEditing={()=>{
                      calculateBMI();
                    }}
                  />
                </View>
                <Text>BMI</Text>
                <View style={UtilsStyles.action}>
                  <TextInput 
                    editable={false}
                    placeholderTextColor="#666666"
                    style={[UtilsStyles.textInput]}
                    autoCapitalize="none"
                    value={userInfo.bmi != null ? String(userInfo.bmi) : ''}
                  />
                </View>
                <Text>รอบเอว</Text>
                <View style={UtilsStyles.action}>
                  <TextInput 
                    keyboardType="numeric"
                    placeholder="เซนติเมตร"
                    placeholderTextColor="#666666"
                    style={[UtilsStyles.textInput]}
                    autoCapitalize="none"
                    value={userInfo.waistline != null ? String(userInfo.waistline) : ''}
                    onChangeText={(text)=>{
                      if(text === ''){
                        setUserInfo({...userInfo, waistline: ''})
                      } else if (/^\d+\.?\d*$/.test(text)) {
                        setUserInfo({...userInfo, waistline: text})
                      }
                    }}
                  />
                </View>
                <Text>อัตราการกรองของไต</Text>
                <View style={UtilsStyles.action}>
                  <TextInput
                    placeholderTextColor="#666666"
                    style={[UtilsStyles.textInput]}
                    autoCapitalize="none"
                    value={userInfo.scr}
                    onChangeText={(text)=>{
                      setUserInfo({...userInfo, scr: text})
                    }}
                  />
                </View>
                <Text>ระยะไต</Text>
                <View style={UtilsStyles.action}>
                  <TextInput
                    placeholderTextColor="#666666"
                    style={[UtilsStyles.textInput]}
                    autoCapitalize="none"
                    value={userInfo.kidney_stage}
                    onChangeText={(text)=>{
                      setUserInfo({...userInfo, kidney_stage: text})
                    }}
                  />
                </View>
                {
                  screenMode == "E" && (
                  <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text>รูปผู้ใช้งาน</Text>
                      <Button 
                        title="เปลี่ยน" 
                        color="#81E8B2" 
                        onPress={() => {
                          onChangeProfilePicture();
                        }}>
                      </Button>
                    </View>
                  )
                }
              </View>
            )}
            {selectedIndex === 1 && (
              <View>
                {
                  screenMode == "E" && (
                    <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                      <View style={{flexBasis: '25%'}}>
                        <Button 
                          title="+ เพิ่ม" 
                          color="#81E8B2" 
                          onPress={() => {
                            setModalDiseaseVisible({show: true, flag: "A"});
                          }}>
                        </Button>
                      </View>
                    </View>
                  )
                }
                <DataTable>
                  <DataTable.Header>
                    <DataTable.Title>ชื่อโรคประจำตัว</DataTable.Title>
                  </DataTable.Header>
                  <ScrollView>
                    {
                      diseaseList.map((item, key) => {
                        return(
                            <TouchableOpacity key={key} onPress={() => {
                              setModalDiseaseVisible({show: true, flag: "E"});
                              setDisease(item);
                            }}>
                              <DataTable.Row key={key}>
                                <DataTable.Cell>{item.disease_name}</DataTable.Cell>
                              </DataTable.Row>
                            </TouchableOpacity>
                          )
                      })
                    }
                  </ScrollView>
                </DataTable>
              </View>
            )}
            {selectedIndex === 2 && (
              <View>
                {
                  screenMode == "E" && (
                    <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                      <View style={{flexBasis: '25%'}}>
                        <Button 
                          title="+ เพิ่ม" 
                          color="#81E8B2" 
                          onPress={() => {
                            setModalMedicineVisible({show: true, flag: "A"})
                          }}>
                        </Button>
                      </View>
                    </View>
                  )
                }
                <DataTable>
                  <DataTable.Header>
                    <DataTable.Title>ชื่อยาประจำตัว</DataTable.Title>
                  </DataTable.Header>
                  <ScrollView>
                    {
                      medicineList.map((item, key) => {
                        return(
                            <TouchableOpacity key={key} onPress={() => {
                                setModalMedicineVisible({show: true, flag: "E"});
                                setMedicine(item);
                              }}>
                              <DataTable.Row key={key}>
                                <DataTable.Cell>{item.medicine_name_th}</DataTable.Cell>
                              </DataTable.Row>
                            </TouchableOpacity>
                          )
                      })
                    }
                  </ScrollView>
                </DataTable>
              </View>
            )}
            {
              screenMode == "E" && (
                <View style={{ marginTop: 20 }}>
                  <Button
                    title="บันทึก"
                    color="#81E8B2"
                    onPress={() => {
                      updateUserInfo();
                    }}
                  />
                </View>
              )
            }
            {
              screenMode == "V" && (
                <View style={{ marginTop: 20 }}>
                  <Button
                    title="บันทึกแก้ไข"
                    color="#81E8B2"
                    onPress={() => {
                      updateUserInfo();
                    }}
                  />
                </View>
              )
            }
            {
              screenMode == "V" && (
                <View style={{ marginTop: 20 }}>
                  <Button
                    title="ลบผู้ใช้งาน"
                    color="#f44336"
                    onPress={() => {
                      deleteUserConfirmAlert();
                    }}
                  />
                </View>
              )
            }
          </ScrollView>
        </View>
      </SafeAreaView>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  emptyView: {
    paddingHorizontal: 10, 
    flexBasis: '25%', 
    backgroundColor: '#81E8B2'
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
    backgroundColor: 'transparent'
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  customInputText: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
    width: "100%"
  }
});
