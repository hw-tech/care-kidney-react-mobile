import React, { useState } from 'react';
import { StyleSheet, Button, TextInput, Image } from 'react-native';

import { Text, View } from '../components/Themed';
import { LoginStyles, UtilsStyles } from '../constants/CustomStyles';
import { useTheme } from 'react-native-paper';
import { Ionicons } from '@expo/vector-icons';

import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { ROOT_URL } from '../constants/Variable';
import UserInfo  from '../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';

export default function LoginScreen({ navigation } : { navigation: any }) {
  const [spinner, setSpinner] = useState(false);
  const [userObj, setUserObj] = React.useState({
    username: "",
    password: "",
  });

  const onLogin = async () => {
    if(!userObj.username || !userObj.password){
      Toast.show('กรุณากรอกชื่อผู้ใช้งานและรหัสผ่าน', Toast.SHORT);
      return;
    }

    setSpinner(true);

    await axios.get(`${ROOT_URL}/users/login/${userObj.username}/${userObj.password}`)
    .then(response => {
      if(response.status == 200){
        if(response.data.length == 0){
          setSpinner(false);
          Toast.show('ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง', Toast.SHORT);
        } else {
          let item: UserInfo = response.data[0];
          // const jsonValue = JSON.stringify(item);
          // AsyncStorage.setItem('userInfo', jsonValue);
          // navigation.replace('Home');

          // Get more information
          axios.get(`${ROOT_URL}/users/info/${item.user_id}/${item.admin_flag}`).then(response => {
            let temp: UserInfo = new UserInfo();
            temp = response.data.userInfo[0];
            temp.disease = response.data.disease;
            temp.medicine = response.data.medicine;
            const jsonValue = JSON.stringify(temp);

            AsyncStorage.setItem('userInfo', jsonValue);
            setSpinner(false);
            navigation.replace('Home');
          });
        }
      }
    }, (error) => {
      console.log(error);
      setSpinner(false);
    });
  };

  const { colors } = useTheme();

  return (
    
    <View style={LoginStyles.loginContainer}>

      <Spinner
        visible={spinner}
        textContent={'กรุณารอสักครู่ ...'}
        textStyle={{color: '#FFF'}}
      />

      <View style={LoginStyles.loginContent}>
        <Image
          style={{ width: 150, height: 150, alignSelf: 'center', marginBottom: 10 }}
          source={require('./../assets/images/kidney-title.png')}
        />
        <Text>ชื่อผู้ใช้งาน</Text>
        <View style={UtilsStyles.action}>
          <Ionicons name="ios-person" size={30} style={{ marginBottom: -3 }}/>
          <TextInput 
            placeholderTextColor="#666666"
            style={[UtilsStyles.textInput, {
                color: colors.text
            }]}
            autoCapitalize="none"
            onChangeText={(text)=>{
              setUserObj({...userObj, username: text})
            }}
          />
        </View>
        <Text>รหัสผ่าน</Text>
        <View style={UtilsStyles.action}>
          <Ionicons name="ios-lock" size={30} style={{ marginBottom: -3 }}/>
          <TextInput 
            secureTextEntry={true}
            placeholderTextColor="#666666"
            style={[UtilsStyles.textInput, {
                color: colors.text
            }]}
            autoCapitalize="none"
            onChangeText={(text)=>{
              setUserObj({...userObj, password: text})
            }}
          />
        </View>
        <View style={{ padding: 10 }}>
          <Button
            title="เข้าสู่ระบบ"
            color="#81E8B2"
            onPress={onLogin}
          />
        </View>
        <View style={styles.registerView}>
          <Text>คุณยังไม่มีบัญชีผู้ใช้งานใช่หรือไม่ ? </Text>
          <Text style={{color: '#81E8B2' }}
            onPress={() => navigation.push('Register')}>
            สมัครสมาชิก
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  registerView: {
    flexDirection: "row",
     paddingTop: 20
  },
});