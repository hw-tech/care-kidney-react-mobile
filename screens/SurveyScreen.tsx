import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList } from 'react-native';

import { Text, View } from '../components/Themed';
import { Ionicons } from '@expo/vector-icons';
import { Card, Title } from 'react-native-paper';
import { Linking } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { ContentStyles } from '../constants/CustomStyles';
import { StatusBar } from 'react-native';

import UserInfo from '../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
export default function SurveyScreen({ navigation }: { navigation: any }) {
  const numColumns = 1;
  const picsumImages = [
    { key: '1', pic: require('./../assets/images/kidneyPExtra.png'), path: 'Survey.Patient' },
    { key: '2', pic: require('./../assets/images/kidneyPNormal.png'), path: 'Survey.General' },
  ];

  const [images, setImages] = React.useState(picsumImages);
  const [userInfo, setUserInfo] = React.useState(new UserInfo());
  useEffect(() => {
    getData().then(
      (data) => {
        setUserInfo(data);
      },
      (err) => {
        console.log(err);
      }
    );
  }, []);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log(e);
    }
  }
  function renderItem({ item }: { item: any }) {
    if (item.key == '1') {
      return (
        <TouchableOpacity onPress={() => { navigation.push(item.path) }} >
          <Image
            style={{ flex: 1, marginTop: 5, height: 100, width: "auto", padding: 2 }}
            source={item.pic}
            resizeMode="stretch"
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity onPress={() => { Linking.openURL('https://med.mahidol.ac.th/kidney_disease_risk/Thai_CKD_risk_score/Thai_CKD_risk_score.html') }} >
          <Image
            style={{ flex: 1, marginTop: 20, height: 100, width: "auto", padding: 2 }}
            source={item.pic}
            resizeMode="stretch"
          />
        </TouchableOpacity>
      );
    }

  }

  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 60, height: 60, alignSelf: 'center' }}
              source={require('./../assets/images/kidney-title.png')}
            />
          </View>
          <View style={styles.emptyView}></View>
        </View>
        <View style={ContentStyles.menuImg}>
          <Image
            style={{ width: 75, height: 75 }}
            source={require('./../assets/images/menu/menu-servey.png')}

          />
          <Text style={ContentStyles.titleText}>ประเมินสุขภาพไต</Text>
        </View>
        <View style={styles.content}>
          <FlatList data={images} renderItem={renderItem} numColumns={numColumns} />
        </View>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  backButtonView: {
    paddingLeft: 20,
    flexBasis: '25%'
  },
  imgHeaderView: {
    flexBasis: '50%',
    backgroundColor: '#81E8B2'
  },
  imgHeaderViewSx: {
    flexBasis: '25%',
    backgroundColor: '#81E8B2',
    paddingRight: 20
  },
  emptyView: {
    paddingHorizontal: 10,
    flexBasis: '25%',
    backgroundColor: '#81E8B2'
  },
  content: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingHorizontal: 30
  }
});
