import React, { useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList, StatusBar, TouchableOpacity, Alert } from 'react-native';
import { useIsFocused } from "@react-navigation/native";

import { Text, View } from '../components/Themed';
import { Card, Title } from 'react-native-paper';
import { ContentStyles, UtilsStyles } from '../constants/CustomStyles';
import { Ionicons } from '@expo/vector-icons';

import UserInfo from '../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';

export default function HomeScreen({ navigation } : { navigation: any }) {
  const numColumns = 2;
  const isVisible = useIsFocused();

  var picsumImages = [];
  
  const [images, setImages] = React.useState([{}]);
  const [userInfo, setUserInfo] = React.useState(new UserInfo());

  useEffect(() => {
    getData().then(
      (data) => {

        // admin_flag Y = admin | N = user

        if(data != null && data.admin_flag === 'N'){
          picsumImages = [  
            {key: 1, name: 'เครื่องปรุง', pic: require('./../assets/images/menu/menu-incrediant.png'), path: 'GarnishList'},
            {key: 2, name: 'เมนูอาหาร', pic: require('./../assets/images/menu/menu-food_type.png'), path: 'FoodMenuTypeList'},
            {key: 3, name: 'บันทึกการรับประทานอาหาร', pic: require('./../assets/images/menu/menu-food_type2.png'), path: 'Consumption'},
            {key: 4, name: 'ประเมินสุขภาพไต', pic: require('./../assets/images/menu/menu-servey.png'), path: 'Survey'},
            {key: 5, name: 'ประวัติการรับประทานอาหาร', pic: require('./../assets/images/menu/menu-save.png'), path: 'History'},
            {key: 6, name: 'ข้อมูลดูแลสุขภาพไต', pic: require('./../assets/images/menu/menu-ex.png'), path: 'KidneyInfo'},
          ];
        } else {
          picsumImages = [  
            {key: 1, name: 'เครื่องปรุง', pic: require('./../assets/images/menu/menu-incrediant.png'), path: 'GarnishList'},
            {key: 2, name: 'ประเภทอาหาร', pic: require('./../assets/images/menu/menu-food_type2.png'), path: 'FoodTypeList'},
            {key: 3, name: 'เมนูอาหาร', pic: require('./../assets/images/menu/menu-food_type.png'), path: 'FoodMenuTypeList'},
            {key: 4, name: 'ข้อมูลผู้ใช้งาน', pic: require('./../assets/images/menu/menu-user.png'), path: 'UserScreen'},
            {key: 5, name: 'ข้อมูลผู้ดูแลระบบ', pic: require('./../assets/images/menu/menu-admin.png'), path: 'UserAdminScreen'},
            {key: 6, name: 'ประวัติการรับประทานอาหาร', pic: require('./../assets/images/menu/menu-save.png'), path: 'HistoryAdmin'},
            // {key: 6, name: 'สูตรอาหารบำรุงไต', pic: require('./../assets/images/food.png'), path: 'FoodInfo'},
          ];
        }
        
        if (isVisible) {
          setUserInfo(data);
          setImages(picsumImages);
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }, [isVisible]);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('userInfo')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      console.log(e);
    }
  }

  const confirmLogoutAlert = () =>{
    Alert.alert(
      "แจ้งเตือน",
      "คณต้องการออกจากระบบหรือไม่ ?",
      [
        {
          text: "ยกเลิก",
          onPress: () => {},
          style: "cancel"
        },
        { 
          text: "ตกลง", 
          onPress: () => {
            AsyncStorage.removeItem('userInfo');
            AsyncStorage.removeItem('userSelected');
            navigation.push('Login');
          }
        }
      ],
      { cancelable: false }
    );
  }

  function renderItem({ item } : { item: any }) {
    return (
      // <Card style={{ flex: 1, margin: 10}} onPress={() => navigation.push(item.path)}>
      //   <Card.Content style={{height: 90}}>
      //     <Title style={{fontSize: 18}}>{item.name}</Title>
      //   </Card.Content>
      //   <Card.Cover source={item.pic} style={{ height: 150, width: "auto", padding: 25}}/>
      // </Card>
      <TouchableOpacity style={{ flex: 1, margin: 20, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center'}} onPress={() => {
          if(item.path !== ''){navigation.push(item.path)}
        }
      }>
        <Image
          style={{ height: 100, width: 100, padding: 50}}
          source={item.pic}
        />
        <Text style={{fontSize: 20, fontWeight: '600', color: '#000', paddingTop: 10,textAlign:'center'}}>{item.name}</Text>
      </TouchableOpacity>
    );
  }

  return (
    <React.Fragment>
      <SafeAreaView style={ContentStyles.topSafeAreaView} />
      <SafeAreaView style={ContentStyles.maincontainer}>
        <StatusBar backgroundColor="#81E8B2" />
        <View style={ContentStyles.customNavBar}>
          <Ionicons name="ios-close" size={40} style={styles.closeButtonView} onPress={() => confirmLogoutAlert()}/>
          <View style={{ flexBasis: '25%', backgroundColor: '#5284bd', justifyContent: 'center', alignSelf: 'center', padding: 5, borderRadius: 30 }}>
            <Text style={{textAlign: 'center', color: '#FFF'}}>{userInfo != null ? userInfo.firstname : ""}</Text>
          </View>
          <View style={styles.imgHeaderView}>
            <Image
              style={{ width: 60, height: 60, alignSelf: 'center' }}
              source={require('./../assets/images/kidney-title.png')}
            />
          </View>
          <TouchableOpacity style={styles.imgHeaderViewSx} onPress={()=> {
              if(userInfo.admin_flag === 'Y'){
                navigation.push('Profile.Admin', {item: "ADMIN_USER"});
              } else {
                navigation.push('Profile', {item: "ADD_USER"});
              }
            }
          }>
            <Image
              style={{ width: 40, height: 40, alignSelf: 'flex-end', borderRadius: 40/2 }}
              source={userInfo.image_path == "" || userInfo.image_path == null ?  require('./../assets/images/account.png') : {uri: userInfo.image_path}}
            />
          </TouchableOpacity>
        </View>
        <View style={ContentStyles.menuImgLeftSide}>
          <View style={{flexDirection: 'row', backgroundColor: 'transparent', paddingHorizontal: 20}}>
            <Image
              style={{ width: 150, height: 150 }}
              source={require('./../assets/images/menu/menu-header.png')}
            />
            <View style={{flexDirection:'row', flexWrap: 'wrap', flexShrink: 1, backgroundColor: 'transparent'}}>
              <View style={{backgroundColor: 'transparent'}}>
                <Text style={{fontSize: 25, fontWeight: 'bold', color: '#FFF', textAlign: 'center'}}>Care kidney</Text>
                <Text style={{fontSize: 20, fontWeight: 'bold', color: '#FFF', textAlign: 'center'}}>ผู้ช่วยดูแลสุขภาพไตของคุณ</Text>
              </View>
              {/* <View style={{width: '100%', marginStart: 50, borderBottomLeftRadius: 30, borderTopLeftRadius: 30, padding: 10, marginVertical: 10, alignItems: 'center'}}>
                <Text style={{fontSize: 15, color: '#000'}}>{userInfo != null ? userInfo.firstname : ""}</Text>
              </View> */}
            </View>
          </View>
        </View>
        <View style={ContentStyles.contentAbsolute}>
          <View style={{padding:0, backgroundColor: 'transparent'}}>
            <FlatList data={images} renderItem={renderItem} numColumns={numColumns} keyExtractor={(item, index) => index.toString()} />
          </View>
        </View>
      </SafeAreaView>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  emptyView: {
    paddingHorizontal: 10, 
    flexBasis: '25%', 
    backgroundColor: '#81E8B2'
  },
  imgHeaderView: {
    flexBasis: '20%',
    backgroundColor: '#81E8B2'
  },
  imgHeaderViewSx: {
    flexBasis: '40%',
    backgroundColor: '#81E8B2',
    paddingRight: 20
  },
  closeButtonView: {
    paddingLeft: 20,
    flexBasis: '15%'
  },
});
