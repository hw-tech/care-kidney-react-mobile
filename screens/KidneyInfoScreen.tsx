import React, { useState, useEffect } from "react";
import {
	StyleSheet,
	Image,
	SafeAreaView,
	ScrollView,
	StatusBar,
	Modal,
} from "react-native";

import { Text, View } from "../components/Themed";
import { ContentStyles, UtilsStyles } from "../constants/CustomStyles";

import { Ionicons } from "@expo/vector-icons";
import { DataTable } from "react-native-paper";

export default function KidneyInfoScreen({
	route,
	navigation,
}: {
	route: any;
	navigation: any;
}) {
	var itemList = [
		{
			key: 1,
			name: "การดูแลสุขภาพครอบครัวเพื่อชะลอไตเสื่อม",
			img: require("./../assets/images/discliption/master1.png"),
		},
		{
			key: 2,
			name: "สาเหตุของโรคไตเรื้อรังที่พบบ่อย",
			img: require("./../assets/images/discliption/master2.png"),
		},
		{
			key: 3,
			name: "อาการของโรคไตเรื้อรัง",
			img: require("./../assets/images/discliption/master3.png"),
		},
		{
			key: 4,
			name: "ชะลอการเสื่อมของไตเสื่อมทำอย่างไร",
			img: require("./../assets/images/discliption/master4.png"),
		},
		{
			key: 5,
			name: "คุมความดันโลหิตและเบาหวาน",
			img: require("./../assets/images/discliption/master5.png"),
		},
		{
			key: 6,
			name: "ดูแลเรื่องอาหารการกินและการดื่ม",
			img: require("./../assets/images/discliption/master6.png"),
		},
		{
			key: 7,
			name: "ดูแลเรื่องการใช้ยา",
			img: require("./../assets/images/discliption/master7.png"),
		},
		{
			key: 8,
			name: "ออกกำลังกาย",
			img: require("./../assets/images/discliption/master8.png"),
		},
	];

	const [itemShow, setItemShow] = React.useState({
		key: 0,
		name: "",
		img: require("./../assets/images/discliption/master1.png"),
	});

	const [modalDiseaseVisible, setModalDiseaseVisible] = useState({
		show: false,
	});

	return (
		<React.Fragment>
			<SafeAreaView style={ContentStyles.topSafeAreaView} />
			<SafeAreaView style={ContentStyles.maincontainer}>
				<StatusBar backgroundColor="#81E8B2" />
				<View style={ContentStyles.customNavBar}>
					<Ionicons
						name="ios-arrow-back"
						size={40}
						style={styles.backButtonView}
						onPress={() => navigation.goBack()}
					/>
					<View style={styles.imgHeaderView}>
						<Image
							style={{ width: 60, height: 60, alignSelf: "center" }}
							source={require("./../assets/images/kidney-title.png")}
						/>
					</View>
					<View style={styles.emptyView}></View>
				</View>
				<View style={ContentStyles.menuImg}>
					<Image
						style={{ width: 75, height: 75 }}
						source={require("./../assets/images/menu/menu-ex.png")}
					/>
					<Text style={ContentStyles.titleText}>ข้อมูลดูแลสุขภาพไต</Text>
				</View>
				<View style={ContentStyles.whiteBackgroundColor}>
					<Modal
						animationType="slide"
						transparent={false}
						visible={modalDiseaseVisible.show}
					>
						<ScrollView contentContainerStyle={{ flexGrow: 1 }}>
							<View style={{ alignItems: "flex-end", paddingHorizontal: 20 }}>
								<Ionicons
									name="ios-close"
									size={40}
									onPress={() =>
										setModalDiseaseVisible({
											...modalDiseaseVisible,
											show: false,
										})
									}
								/>
							</View>
								<Text
									style={{
										fontSize: 18,
										fontWeight: "bold",
										alignSelf: "center",
									}}
								>
									{itemShow.name}
								</Text>
							<View
								style={{
									flex: 1,
									flexDirection: "row",
									marginLeft: "13%",
									justifyContent: "space-around",
								}}
							>
								
								<View
									style={{
										width: "90%",
										marginRight: 50,
										borderColor: "#000",
										borderBottomLeftRadius: 30,
										borderBottomRightRadius: 30,
										borderTopLeftRadius: 30,
										borderTopRightRadius: 30,
									}}
								>
									{itemShow.img !== "" && (
											<View>
												<Image
													style={itemShow.key == 6 ||itemShow.key == 8 == true? styles.imgStyle1:styles.imgStyle2 }
													key={itemShow.key}
													source={itemShow.img}
													resizeMode = 'contain'
												/>
											</View>
									)}
								</View>
								
							</View>
						</ScrollView>
					</Modal>

					<DataTable style={{ flex: 1 }}>
						<ScrollView>
							{itemList.map((item, key) => {
								return (
									<DataTable.Row
										key={key}
										onPress={() => {
											setModalDiseaseVisible({ show: true });
											setItemShow(item);
										}}
									>
										<DataTable.Cell>{item.name}</DataTable.Cell>
									</DataTable.Row>
								);
							})}
						</ScrollView>
					</DataTable>
				</View>
			</SafeAreaView>
		</React.Fragment>
	);
}

const styles = StyleSheet.create({
	taxTitle: {
		fontSize: 25,
		fontWeight: "bold",
		color: "#FFF",
		textAlign: "center",
	},
	backButtonView: {
		paddingLeft: 20,
		flexBasis: "25%",
	},
	imgHeaderView: {
		flexBasis: "50%",
		backgroundColor: "#81E8B2",
	},
	emptyView: {
		paddingHorizontal: 10,
		flexBasis: "25%",
		backgroundColor: "#81E8B2",
	},
	floatingBtn: {
		position: "absolute",
		margin: 16,
		right: 0,
		bottom: 0,
		backgroundColor: "#81E8B2",
		width: 50,
		height: 50,
		borderRadius: 100,
		justifyContent: "center",
		alignItems: "center",
		zIndex: 10000,
	},
	emptyViewModal: {
		paddingHorizontal: 10,
		flexBasis: "75%",
		backgroundColor: "#FFF",
	},
	imgStyle1:{
		width: 380,
		height: 1100,
		alignSelf: "center"
	},
	
	imgStyle2:{
		width: 380,
		height: 600,
		alignSelf: "center"
	},
});
