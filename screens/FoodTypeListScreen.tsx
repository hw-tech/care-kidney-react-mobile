import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList, ScrollView, StatusBar, TouchableOpacity, Alert, Button } from 'react-native';
import { SearchBar, Overlay } from 'react-native-elements';
import { Text, View } from '../components/Themed';
import { Card, Title } from 'react-native-paper';
import { ContentStyles, UtilsStyles } from '../constants/CustomStyles';
import { Ionicons } from '@expo/vector-icons';
import axios from 'axios';
import UserInfo from '../data-model/UserInfo';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import { ROOT_URL } from '../constants/Variable';
import Spinner from 'react-native-loading-spinner-overlay';

export default function FoodTypeListScreen({ navigation }: { navigation: any }) {
    const numColumns = 2;
    const [userInfo, setUserInfo] = React.useState(new UserInfo());
    // var userInfo: UserInfo;
    const [foodTypeObj, setFoodTypeObj] = useState([]);
    const [spinner, setSpinner] = useState(false);

    useEffect(() => {
        getData().then(
            (data) => {
                setUserInfo(data);
                getAllFoodsType();
            },
            (err) => {
                console.log(err);
            }
        );
    }, []);

    const getAllFoodsType = async () => {
        setSpinner(true);
        await axios.get(`${ROOT_URL}/foodtype/`)
            .then(response => {
                if (response.status == 200) {
                    setFoodTypeObj(response.data);
                    // console.log(response.data);
                    setSpinner(false);
                }
            }, (error) => {
                Toast.show(error, Toast.SHORT)
                setSpinner(false);
            });
    }

    const getData = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('userInfo')
            return jsonValue != null ? JSON.parse(jsonValue) : null;
        } catch (e) {
            console.log(e);
        }
    }

    function renderButtonAdd() {
        if (userInfo.admin_flag !== 'N') {
            return (
                <TouchableOpacity style={{
                    position: 'absolute',
                    margin: 15,
                    right: 0,
                    bottom: 0,
                    backgroundColor: "#81E8B2",
                    width: 50,
                    height: 50,
                    borderRadius: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                    zIndex: 10000
                }}
                    onPress={() => navigation.push('AddFoodType')}
                >
                    <Text style={{ fontSize: 30, color: '#fff' }}>+</Text>
                </TouchableOpacity>
            );
        }
    }

    function renderItem({ item }: { item: any }) {
        return (
            <TouchableOpacity style={{ flex: 1, margin: 20, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }} onPress={() => navigation.push('FoodTypeDetail', {
                itemId: item.food_type_id,
                itemNameTH: item.food_type_name_th,
                itemNameEN: item.food_type_name_en,
                itemImg: item.food_type_image_url
            })}>

                <Card.Cover source={{ uri: item.food_type_image_url }} style={{ height: 130, width: 130, padding: 3, marginTop: 3 }} />
                <View style={{ flex: 0.8, flexDirection: 'column', justifyContent: 'center', alignSelf: "center", flexWrap: 'wrap' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#f5b43b', paddingTop: 10 }}>{item.food_type_name_th}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    return (
        <React.Fragment>
            <SafeAreaView style={ContentStyles.topSafeAreaView} />
            <SafeAreaView style={ContentStyles.maincontainer}>
                <Spinner
                    visible={spinner}
                    textContent={'กรุณารอสักครู่ ...'}
                    textStyle={{ color: '#FFF' }}
                />
                <StatusBar backgroundColor="#81E8B2" />
                <View style={ContentStyles.customNavBar}>
                    <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()} />
                    <View style={styles.imgHeaderView}>
                        <Image
                            style={{ width: 60, height: 60, alignSelf: 'center' }}
                            source={require('./../assets/images/kidney-title.png')}
                        />
                    </View>
                    <View style={styles.emptyView}></View>
                </View>
                <View style={ContentStyles.menuImg}>
                    <Image
                        style={{ width: 75, height: 75 }}
                        source={require('./../assets/images/food.png')}
                    />
                    <Text style={ContentStyles.titleText}>ประเภทอาหาร</Text>
                </View>
                {renderButtonAdd()}
                <View style={ContentStyles.contentAbsolute}>
                    <View style={{ padding: 10, backgroundColor: 'transparent' }}>
                        <FlatList data={foodTypeObj} renderItem={renderItem} numColumns={numColumns} keyExtractor={(item, index) => index.toString()} />
                    </View>
                </View>
            </SafeAreaView>
            <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
        </React.Fragment>
    );
}

const styles = StyleSheet.create({
    backButtonView: {
        paddingLeft: 20,
        flexBasis: '25%'
    },
    imgHeaderView: {
        flexBasis: '50%',
        backgroundColor: '#81E8B2'
    },
    imgHeaderViewSx: {
        flexBasis: '25%',
        backgroundColor: '#81E8B2',
        paddingRight: 20
    },
    emptyView: {
        paddingHorizontal: 10,
        flexBasis: '25%',
        backgroundColor: '#81E8B2'
    },
});
