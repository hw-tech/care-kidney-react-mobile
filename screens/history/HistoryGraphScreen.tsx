import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList, StatusBar, TouchableOpacity, Dimensions } from 'react-native';

import { Text, View } from '../../components/Themed';
import { Ionicons } from '@expo/vector-icons';
import { ContentStyles, UtilsStyles } from '../../constants/CustomStyles';

import Moment from 'moment';
import 'moment/min/locales'

import { LineChart } from "react-native-chart-kit";

export default function HistoryGraphScreen({ route, navigation } : { route: any, navigation: any }) {
    const [graph, setGraph] = React.useState(Array());
    const [monthDisplay, setMonthDisplay] = React.useState(Array());

    const [data, setData] = React.useState(
        {
            labels: [] as string[],
            datasets: [
              {
                data: [0],
                color: (opacity = 1) => `rgba(255, 0, 0, 0.75)`, // optional
                strokeWidth: 2, // optional
              }
            ],
            legend: ["การบริโภค"], // optional
        }
    );

    useEffect(() => {
        let isMounted = true;
        if (isMounted){
            setGraph(route.params.item);
            setMonthDisplay(route.params.month);

            let result : string[] = [];
            let result2 : number[] = [];
            for(let item of route.params.item){
                result.push(Moment(item.DateOnly).format('DD'));
                result2.push(item.SumAmount);
            }
            
            setData(
                {
                    labels: result,
                    datasets: [
                      {
                        data: result2,
                        color: (opacity = 1) => `rgba(255, 0, 0, 0.75)`, // optional
                        strokeWidth: 2 // optional
                      }
                    ],
                    legend: ["การบริโภค"] // optional
                }
            );
        }
    
        return () => { isMounted = false };
      }, []);
    
    return (
        <React.Fragment>
            <SafeAreaView style={ContentStyles.topSafeAreaView} />
            <SafeAreaView style={ContentStyles.maincontainer}>
                <StatusBar backgroundColor="#81E8B2" />
                <View style={ContentStyles.customNavBar}>
                    <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()}/>
                    <View style={styles.imgHeaderView}>
                        <Image
                            style={{ width: 40, height: 40, alignSelf: 'center' }}
                            source={require('./../../assets/images/kidney-title.png')}
                        />
                    </View>
                    <View style={styles.emptyView}></View>
                </View>
                <View style={styles.menuImg}>
                    <Image
                        style={{ width: 75, height: 75 }}
                        source={require('./../../assets/images/food.png')}
                    />
                    <Text style={ContentStyles.titleText}>{`เดือน ${monthDisplay}`}</Text>
                </View>
                <View style={{backgroundColor: '#FFFFFF', padding: 10, flex: 1}}>
                    <LineChart
                        data={data}
                        width={Dimensions.get("window").width - 20}
                        height={300}
                        verticalLabelRotation={30}
                        chartConfig={{
                            backgroundColor: "#9aecc1",
                            backgroundGradientFrom: "#9aecc1",
                            backgroundGradientTo: "#ccf5e0",
                            decimalPlaces: 2, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(0, 0, 0, 0.5)`,
                            labelColor: (opacity = 1) => `rgba(0, 0, 0, 1)`,
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#ffa726"
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />
                    <View style={{paddingHorizontal: 10, paddingVertical: 5}}>
                        <Text>แนวตั้งคือ ปริมาณโซเดียมต่อวัน (หน่วย:มิลลิกรัม)</Text>
                        <Text>แนวนอนคือ วันที่รับประทาน (หน่วย:วัน)</Text>
                        <Text style={{color: 'red'}}>* ไม่ควรเกิน 2000 mg ต่อวัน</Text>
                    </View>
                </View>
            </SafeAreaView>
        </React.Fragment>
    );
}

const styles = StyleSheet.create({
    backButtonView: {
      paddingLeft: 20,
      flexBasis: '25%'
    },
    imgHeaderView: {
      flexBasis: '50%',
      backgroundColor: '#81E8B2'
    },
    emptyView: {
      paddingHorizontal: 10, 
      flexBasis: '25%', 
      backgroundColor: '#81E8B2'
    },
    subText: {
      fontSize: 15, 
      color: '#FFF',
      alignItems: 'center'
    },
    valueTextBorder: {
      fontSize: 20, 
      fontWeight: 'bold', 
      backgroundColor: '#fdfd96',
      color: '#81E8B2',
      alignItems: 'center',
      paddingHorizontal: 20,
      paddingVertical: 10,
      borderRadius: 20
    },
    menuImg: {
      backgroundColor: '#81E8B2', 
      borderBottomLeftRadius: 150, 
      borderBottomRightRadius: 150, 
      justifyContent: 'flex-start', 
      alignItems: 'center', 
      paddingBottom: 30, 
    },
});
  