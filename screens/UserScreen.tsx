import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, ScrollView, StatusBar, TouchableOpacity } from 'react-native';
import { useIsFocused } from "@react-navigation/native";

import { Text, View } from '../components/Themed';
import { ContentStyles, UtilsStyles } from '../constants/CustomStyles';

import { Ionicons } from '@expo/vector-icons';

import UserInfo from '../data-model/UserInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { DataTable } from 'react-native-paper';
// 1. import spinner
import Spinner from 'react-native-loading-spinner-overlay';

import axios from 'axios';
import { ROOT_URL } from '../constants/Variable';
import ImagePicker from 'react-native-image-picker';

export default function UserScreen({ route, navigation } : { route: any, navigation: any }) {
    const isVisible = useIsFocused();

    // 2. สร้างตัวแปรสำหรับ เปิด/ปิด spinner [ข้อ 3 อยู่บรรทัดที่ 98]
    const [spinner, setSpinner] = useState(false);
    
    const [userList, setUserList] = React.useState(Array());

    useEffect(() => {
        if (isVisible) {
            getAllUsers();
        }
    }, [isVisible]);

    const getAllUsers = async () => {
        // 4. เปิด spinner ถ้ามีการเรียก service [อยู่ก่อน await axios]
        setSpinner(true);
        await axios.get(`${ROOT_URL}/users/all/N`)
        .then(response => {
            if(response.status == 200){
                setUserList(response.data);
                // 5. ปิด spinner เมื่อได้ข้อมูลแล้ว [สังเกตว่าจะอยู่ใน resCode == 200]
                setSpinner(false);
            }
        }, (error) => {
            console.log(error);
            // 6. ปิด spinner เมื่อเกิด error
            setSpinner(false);
        });
    }

    const onSelectUser = async (item: UserInfo) => {
        setSpinner(true);
        await axios.get(`${ROOT_URL}/users/info/${item.user_id}/N`)
        .then(response => {
            if(response.status == 200){
                // console.log(response.data);

                let temp: UserInfo = new UserInfo();
                temp = response.data.userInfo[0];
                temp.disease = response.data.disease;
                temp.medicine = response.data.medicine;

                const jsonValue = JSON.stringify(temp);
                AsyncStorage.setItem('userSelected', jsonValue);
                setSpinner(false);
                navigation.push('Profile', {item: "EDIT_USER"});
            }
        }, (error) => {
            console.log(error);
            setSpinner(false);
        });
    }

    return (
        <React.Fragment>
            <SafeAreaView style={ContentStyles.topSafeAreaView} />
            <SafeAreaView style={ContentStyles.maincontainer}>
                <StatusBar backgroundColor="#81E8B2" />
                <View style={ContentStyles.customNavBar}>
                    <Ionicons name="ios-arrow-back" size={40} style={styles.backButtonView} onPress={() => navigation.goBack()}/>
                    <View style={styles.imgHeaderView}>
                        <Image
                        style={{ width: 40, height: 40, alignSelf: 'center' }}
                        source={require('./../assets/images/kidney-title.png')}
                        />
                    </View>
                    <View style={styles.emptyView}></View>
                </View>
                <View style={ContentStyles.menuImg}>
                    <Image
                        style={{ width: 75, height: 75 }}
                        source={require('./../assets/images/menu/menu-user.png')}
                    />
                    <Text style={ContentStyles.titleText}>รายชื่อผู้ใช้งาน</Text>
                </View>
                <View style={ContentStyles.whiteBackgroundColor}>

                    {/* 3. เอา spinner มาใส่ไว้ใน [whiteBackgroundColor] */}
                    <Spinner
                        visible={spinner}
                        textContent={'กรุณารอสักครู่ ...'}
                        textStyle={{ color: '#FFF' }}
                    />

                    <DataTable style={{flex: 1}}>
                        {/* <DataTable.Header>
                            <DataTable.Title>ชื่อผู้ใช้งาน</DataTable.Title>
                        </DataTable.Header> */}
                        <ScrollView>
                        {
                            userList.map((item, key) => {
                            return(
                                    <TouchableOpacity key={key} onPress={() => {
                                        onSelectUser(item);
                                    }}>
                                        <DataTable.Row key={key}>
                                            <DataTable.Cell>{item.title_name} {item.firstname} {item.lastname}</DataTable.Cell>
                                        </DataTable.Row>
                                    </TouchableOpacity>
                                )
                            })
                        }
                        </ScrollView>
                    </DataTable>
                </View>
            </SafeAreaView>
        </React.Fragment>
    );
}

const styles = StyleSheet.create({
    backButtonView: {
      paddingLeft: 20,
      flexBasis: '25%'
    },
    imgHeaderView: {
      flexBasis: '50%',
      backgroundColor: '#81E8B2'
    },
    emptyView: {
      paddingHorizontal: 10, 
      flexBasis: '25%', 
      backgroundColor: '#81E8B2'
    },
  });
  