import React, { Component } from 'react'
import { StyleSheet, Button, TextInput, SafeAreaView, ScrollView, Picker, StatusBar, Image } from 'react-native';

import { Text, View } from 'react-native'

export default function testScreen({ navigation }: { navigation: any }) {

        return (
            <View>
                <View style={{ paddingTop: 50 }}>
              <Button
                title="ประเภท"
                color="#81E8B2"
                onPress={() => navigation.push('FoodTypeList')}
              />
            </View>
            <View style={{ paddingTop: 10 }}>
              <Button
                title="เครื่องปรุง"
                color="#81E8B2"
                onPress={() => navigation.push('GarnishList')}
              />
            </View>
            </View>
        )
    
}
