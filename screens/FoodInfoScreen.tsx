import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, SafeAreaView, FlatList } from 'react-native';

import { Text, View } from '../components/Themed';
import { Ionicons } from '@expo/vector-icons';
import { Card, Title } from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';
export default function FoodInfoScreen({ navigation } : { navigation: any }) {
  const numColumns = 2;
  const picsumImages = [  
    {key: 1, name: 'ผัด', pic: require('./../assets/images/food.png'), path: 'FoodInfo.Type'},
    {key: 2, name: 'แกง/ต้ม', pic: require('./../assets/images/food.png'), path: 'FoodInfo.Type'},
    {key: 3, name: 'ทอด', pic: require('./../assets/images/food.png'), path: 'FoodInfo.Type'},
    {key: 4, name: 'ปิ้ง/ย่าง', pic: require('./../assets/images/food.png'), path: 'FoodInfo.Type'},
    {key: 5, name: 'ตุ๋น/นึ่ง', pic: require('./../assets/images/food.png'), path: 'FoodInfo.Type'},
    {key: 6, name: 'ยำ/พล่า', pic: require('./../assets/images/food.png'), path: 'FoodInfo.Type'},
    {key: 7, name: 'น้ำพริก', pic: require('./../assets/images/food.png'), path: 'FoodInfo.Type'},
    {key: 8, name: 'อาหารจารเดียว', pic: require('./../assets/images/food.png'), path: 'FoodInfo.Type'}
  ]; 

  const [images, setImages] = React.useState(picsumImages);
  const [spinner, setSpinner] = useState(false);
  function renderItem({ item } : { item: any }) {
    return (
      <Card style={{ flex: 1, margin: 10}} onPress={() => navigation.push(item.path)}>
        <Card.Content style={{height: 90}}>
          <Title style={{fontSize: 18}}>{item.name}</Title>
        </Card.Content>
        <Card.Cover source={item.pic} style={{ height: 150, width: "auto", padding: 25}}/>
      </Card>
    );
  }

  return (
    <React.Fragment>
      <SafeAreaView style={{ flex: 0, backgroundColor: '#CE9C6F' }} />
      <View style={{ backgroundColor: '#CE9C6F' }}>
        <Ionicons name="ios-arrow-back" size={40} style={{ paddingLeft: 20 }} onPress={() => navigation.goBack()}/>
      </View>
      <SafeAreaView style={styles.container}>
      <Spinner
          visible={spinner}
          textContent={'กรุณารอสักครู่ ...'}
          textStyle={{ color: '#FFF' }}
        />
        <View style={styles.space}>
          <Image
            style={styles.logo}
            source={require('./../assets/images/food.png')}
          />
          <Text style={styles.title_header}>ข้อมูลอาหาร</Text>
        </View>
        <View style={styles.content}>
          <FlatList data={images} renderItem={renderItem} numColumns={numColumns} />
        </View>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CE9C6F'
  },
  space: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CE9C6F',
    margin: 10,
  },
  content: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 30,
    paddingHorizontal: 30
  },
  title_header: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#FFF',
  },
  logo: {
    width: 100,
    height: 100,
  },
});
