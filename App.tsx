import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';

import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from './screens/LoginScreen';
import BottomTabNavigator from './navigation/BottomTabNavigator';
import Navigation from './navigation';
import RegisterScreen from './screens/RegisterScreen';
import HomeScreen from './screens/HomeScreen';
import HistoryScreen from './screens/HistoryScreen';
import ConsumptionScreen from './screens/ConsumptionScreen';
import FoodInfoScreen from './screens/FoodInfoScreen';
import IngredientInfoScreen from './screens/IngredientInfoScreen';
import ProfileScreen from './screens/ProfileScreen';
import SurveyScreen from './screens/SurveyScreen';
import HistoryDetailScreen from './screens/history/HistoryDetailScreen';
import IngredientDetailScreen from './screens/ingredient/IngredientDetailScreen';
import FoodTypeScreen from './screens/food/FoodTypeScreen';
import FoodDetailScreen from './screens/food/FoodDetailScreen';
import KidneyPatientScreen from './screens/survey/KidneyPatientScreen';
import GeneralPersonScreen from './screens/survey/GeneralPersonScreen';
import FoodTypeListScreen from './screens/FoodTypeListScreen' ;
import AddFoodTypeScreen from './screens/food/AddFoodTypeScreen';
import FoodTypeDetailScreen from './screens/food/FoodTypeDetailScreen' ;
import ManageFoodTypeScreen from './screens/food/ManageFoodTypeScreen';
import ManageGarnishScreen from './screens/ingredient/ManageGarnishScreen';
import GarnishListScreen from './screens/ingredient/GarnishListScreen';
import GarnishDetailScreen from './screens/ingredient/GarnishDetailScreen';
import AddGarnishScreen from './screens/ingredient/AddGarnishScreen';
import testScreenA from './screens/testScreen';
import CreateFoodDetailScreen from './screens/food/CreateFoodDetailScreen';
import ProfileAdminScreen from './screens/ProfileAdminScreen';
import UserScreen from './screens/UserScreen';
import UserAdminScreen from './screens/UserAdminScreen';
import EditFoodDetailScreen from './screens/food/EditFoodDetailScreen';
import FoodMenuTypeListScreen from './screens/food/FoodMenuTypeListScreen';
import KidneyInfoScreen from './screens/KidneyInfoScreen';
import ConsumptionFoodDetailScreen from './screens/consumption/ConsumptionFoodDetailScreen';
import ConsumptionFoodListScreen from './screens/consumption/ConsumptionFoodListScreen';
import ConsumptionFoodMenuScreen from './screens/consumption/ConsumptionFoodMenuScreen';
import HistoryAdminScreen from './screens/HistoryAdminScreen';
import HistoryGraphScreen from './screens/history/HistoryGraphScreen';

const Stack = createStackNavigator();

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerShown: false
            }}
          >
            <Stack.Screen name="Login" component={LoginScreen} />
            {/* <Stack.Screen name="Main" component={BottomTabNavigator} /> */}
            <Stack.Screen name="Register" component={RegisterScreen} />
            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen name="History" component={HistoryScreen} />
            <Stack.Screen name="Consumption" component={ConsumptionScreen} />
            <Stack.Screen name="FoodInfo" component={FoodInfoScreen} />
            <Stack.Screen name="IngredientInfo" component={IngredientInfoScreen} />
            <Stack.Screen name="Profile" component={ProfileScreen} />
            <Stack.Screen name="Profile.Admin" component={ProfileAdminScreen} />
            <Stack.Screen name="Survey" component={SurveyScreen} />
            <Stack.Screen name="History.Detail" component={HistoryDetailScreen} />
            <Stack.Screen name="IngredientInfo.Detail" component={IngredientDetailScreen} />
            <Stack.Screen name="FoodInfo.Type" component={FoodTypeScreen} />
            <Stack.Screen name="FoodInfo.Detail" component={FoodDetailScreen} />
            <Stack.Screen name="Survey.Patient" component={KidneyPatientScreen} />
            <Stack.Screen name="Survey.General" component={GeneralPersonScreen} />
            <Stack.Screen name="FoodTypeList" component={FoodTypeListScreen}/>
            <Stack.Screen name="AddFoodType" component={AddFoodTypeScreen}/>
            <Stack.Screen name="FoodTypeDetail" component={FoodTypeDetailScreen}/>
            <Stack.Screen name="ManageFoodType" component={ManageFoodTypeScreen}/>
            <Stack.Screen name="ManageGarnish" component={ManageGarnishScreen}/>
            <Stack.Screen name="UserScreen" component={UserScreen}/>
            <Stack.Screen name="UserAdminScreen" component={UserAdminScreen}/>
            <Stack.Screen name="EditFoodDetailScreen" component={EditFoodDetailScreen} />
            <Stack.Screen name="KidneyInfo" component={KidneyInfoScreen} />
            <Stack.Screen name="Consumption.FoodMenu" component={ConsumptionFoodMenuScreen} />
            <Stack.Screen name="Consumption.FoodList" component={ConsumptionFoodListScreen} />
            <Stack.Screen name="Consumption.FoodDetail" component={ConsumptionFoodDetailScreen} />
            <Stack.Screen name="HistoryAdmin" component={HistoryAdminScreen} />
            <Stack.Screen name="History.Graph" component={HistoryGraphScreen} />
            
            <Stack.Screen name="GarnishList" component={GarnishListScreen}/>
            <Stack.Screen name="GarnishDetail" component={GarnishDetailScreen}/>
            <Stack.Screen name="AddGarnish" component={AddGarnishScreen}/>
            <Stack.Screen name="testJa" component={testScreenA}/>
            <Stack.Screen name="CreateFoodDetail" component={CreateFoodDetailScreen} />
            <Stack.Screen name="FoodMenuTypeList" component={FoodMenuTypeListScreen} />
          </Stack.Navigator>
        </NavigationContainer>
        <StatusBar />
      </SafeAreaProvider>
    );
  }
}
