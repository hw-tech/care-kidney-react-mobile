export default class Medicine {
    key: number;
    user_id: number;
    medicine_name_th: string;
    medicine_name_en: string;
    medicine_taking: string;
    process: string;

    constructor() {
        this.key = 0;
        this.user_id = 0;
        this.medicine_name_th = "";
        this.medicine_name_en = "";
        this.medicine_taking = "";
        this.process = "";
    }
}