export default class FoodType {
    key: number;
    food_type_id: number;
    food_type_name_th: string;
    food_type_name_en: string;
    image_path: string;
    updateUser:string;
    process: string;

    constructor() {
        this.key = 0;
        this.food_type_id = 0;
        this.food_type_name_th = "";
        this.food_type_name_en = "";
        this.process = "";
        this.image_path = "";
        this.updateUser = "";
    }
}