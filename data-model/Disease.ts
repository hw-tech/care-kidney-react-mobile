export default class Disease {
    key: number;
    user_id: number;
    disease_name: string;
    process: string;

    constructor() {
        this.key = 0;
        this.user_id = 0;
        this.disease_name = "";
        this.process = "";
    }
}