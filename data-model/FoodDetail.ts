export default class FoodDetail {
    key: number;
    food_id: number;
    food_type_id: number;
    food_name_th: string;
    food_name_en: string;
    unit: string;
    sodium: string;
    process: string;
    foodType: any;
    foodTypeName: string;
    updateUser:string;
    image_path:string;

    constructor() {
        this.key = 0;
        this.food_id = 0;
        this.food_type_id = 0;
        this.food_name_th = "";
        this.food_name_en = "";
        this.unit = "";
        this.sodium = "";
        this.process = "";
        this.foodType = "";
        this.foodTypeName = "";
        this.updateUser = "";
        this.image_path = "";
    }
}