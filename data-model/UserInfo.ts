import Disease from '../data-model/Disease';
import Medicine from '../data-model/Medicine';

export default class UserInfo {
    user_login_id: number;
    user_id: number;
    username: string;
    password: string;
    image_name: string;
    image_path: string;
    image_file: string;
    title_name: string;
    firstname: string;
    lastname: string;
    birthdate: Date;
    sex: string;
    identification_id: string;
    weight: string;
    height: string;
    waistline: string;
    bmi: string;
    scr: string;
    kidney_stage: string;
    
    admin_id: number;
    address: string;
    mobile: string;
    email: string;

    admin_flag: string;
    process: string;
    create_user: string;
    update_user: string;

    disease: Disease[];
    medicine: Medicine[];

    constructor() {
        this.user_login_id = 0;
        this.user_id = 0;
        this.username = "";
        this.password = "";
        this.image_name = "";
        this.image_path = "";
        this.image_file = "";
        this.title_name = "";
        this.firstname = "";
        this.lastname = "";
        this.birthdate = new Date();
        this.sex = "";
        this.identification_id = "";
        this.weight = "";
        this.height = "";
        this.waistline = "";
        this.bmi = "";
        this.scr = "";
        this.kidney_stage = "";

        this.admin_id = 0;
        this.address = "";
        this.mobile = "";
        this.email = "";

        this.admin_flag = "";
        this.process = "";
        this.create_user = "";
        this.update_user = "";

        this.disease = Array();
        this.medicine = Array();
    }
}