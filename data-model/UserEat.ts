export default class UserEat {
    key: number;
    user_eat_id: number;
    user_id: number;
    food_id: number;
    food_name: string;
    eat_date: Date;
    eat_amount: string;
    sodium: string;
    process: string;
    create_user: string;

    image_path: string;

    constructor() {
        this.key = 0;
        this.user_eat_id = 0;
        this.user_id = 0;
        this.food_id = 0;
        this.food_name = "";
        this.eat_date = new Date();
        this.eat_amount = "";
        this.sodium = "";
        this.process = "";
        this.create_user = "";

        this.image_path = "";
    }
}