export default class ingredient {
    key: number;
    seasoning_id: number;
    seasoning_name_th: string;
    seasoning_name_en: string;
    sodium: string;
    unit: string;
    process: string;
    seasoning_image_url: string;
    updateUser:string;



    constructor() {
        this.key = 0;
        this.seasoning_id = 0;
        this.seasoning_name_en = "";
        this.seasoning_name_th = "";
        this.sodium = "";
        this.unit = "";
        this.process = "";
        this.seasoning_image_url = "";
        this.updateUser = "";
    }
}