export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  TabHome: undefined;
  TabProfile: undefined;
};

export type TabOneParamList = {
  TabHomeScreen: undefined;
};

export type TabTwoParamList = {
  TabProfileScreen: undefined;
};
